// Gulp.js configuration
'use strict';
const
// source and build folders
    dir = {
        src: 'assets/',
        build: 'dist/'
    },
    // Gulp and plugins
    gulp = require('gulp'),
    gutil = require('gulp-util'),
    newer = require('gulp-newer'),
    imagemin = require('gulp-imagemin'),
    postcss = require('gulp-postcss'),
    deporder = require('gulp-deporder'),
    concat = require('gulp-concat'),
    stripdebug = require('gulp-strip-debug'),
    uglify = require('gulp-uglify'),
    pixelstorem = require('postcss-pixels-to-rem'),
    atImport = require("postcss-import"),
    rucksack = require("rucksack-css"),
    cssnano = require('gulp-cssnano');
// Browser-sync
var browsersync = false;
// image settings
const images = {
    src: dir.src + 'images/**/*',
    build: dir.build + 'images/'
};
const fonts = {
        src: dir.src + 'fonts/**/*',
        build: dir.build + 'fonts/'
    }
    // image processing
gulp.task('images', () => {
    return gulp.src(images.src)
        .pipe(newer(images.build))
        .pipe(imagemin([
            imagemin.jpegtran({ progressive: true }),
            imagemin.gifsicle({ interlaced: true }),
            imagemin.optipng({ optimizationLevel: 5 }),
            imagemin.svgo({ plugins: [{ removeUnknownsAndDefaults: false }, { cleanupIDs: true }, { removeViewBox: true }] })
        ], {
            verbose: true
        }))
        .pipe(gulp.dest(images.build));
});
gulp.task('fonts', () => {
    return gulp.src(fonts.src)
        .pipe(gulp.dest(fonts.build));
});
// CSS settings
var css = {
    src: dir.src + 'styles/main.css',
    watch: dir.src + 'styles/**/*',
    build: dir.build + 'styles/',
    processors: [
        require('postcss-assets')({
            loadPaths: ['images/'],
            basePath: dir.build,
            baseUrl: 'dist/styles'
        }),
        require('postcss-mixins'),
        atImport(),
        pixelstorem(),
        require('postcss-nesting')({}),
        require("lost")(),
        require("postcss-cssnext")(),
        require('precss')(),
        rucksack(),
        require('css-mqpacker')
    ]
};
// CSS processing
gulp.task('css', ['images', 'fonts'], () => {
    return gulp.src(css.src)
        .pipe(postcss(css.processors))
        .pipe(cssnano({
            discardComments: {
                removeAll: true
            }
        }))
        .pipe(gulp.dest(css.build))
        .pipe(browsersync ? browsersync.reload({ stream: true }) : gutil.noop());
});
const jsVendors = {
        src: dir.src + 'scripts/vendors/**/*',
        build: dir.build + 'scripts/vendors/',
        filename: 'vendors.js'
    }
    // JavaScript vendor processing
gulp.task('vendors', () => {
    return gulp.src(jsVendors.src)
        .pipe(deporder())
        .pipe(concat(jsVendors.filename))
        .pipe(stripdebug())
        .pipe(uglify())
        .pipe(gulp.dest(jsVendors.build))
});
// JavaScript settings
const js = {
    src: dir.src + 'scripts/master/**/*',
    build: dir.build + 'scripts/master/',
    filename: 'main.js'
};
const jsWatch = {
        src: dir.src + 'scripts/**/*',
    }
    // JavaScript processing
gulp.task('js', ['vendors'], () => {
    return gulp.src(js.src)
        .pipe(deporder())
        .pipe(concat(js.filename))
        // .pipe(stripdebug())
        .pipe(uglify())
        .pipe(gulp.dest(js.build))
        .pipe(browsersync ? browsersync.reload({ stream: true }) : gutil.noop());
});
// run all tasks
gulp.task('build', ['css', 'js']);
// Browsersync options
const syncOpts = {
    proxy: 'http://192.168.33.10/design-system',
    files: dir.build + '**/*',
    open: false,
    notify: false,
    ghostMode: false,
    ui: {
        port: 3000
    }
};
// browser-sync
gulp.task('browsersync', () => {
    if (browsersync === false) {
        browsersync = require('browser-sync').create();
        browsersync.init(syncOpts);
    }
});
// watch for file changes
gulp.task('watch', ['browsersync'], () => {

    // image changes
    gulp.watch(images.src, ['images']);
    // CSS changes
    gulp.watch(css.watch, ['css']);
    // JavaScript main changes
    gulp.watch(jsWatch.src, ['js']);

});
// default task
gulp.task('default', ['build', 'watch']);