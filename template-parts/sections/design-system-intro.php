<section class="design-system-intro grid">
	<div class="fonts card">
		<h2>Font Families</h2>
		<p>Open Sans</p>
		<p>Open Sans Bold</p>
		<p>Open Sans ExtraBold</p>
	</div>
	<div class="neutrals card">
		<h2>Neutral Colors</h2>
		<p>NeutralOne</p>
		<p>NeutralTwo</p>
		<p>NeutralThree</p>
		<p>NeutralFour</p>
		<p>NeutralFive</p>
	</div>
	<div class="primarys card">
		<h2>Primary Colors</h2>
		<p>PrimaryOne</p>
		<p>PrimaryTwo</p>
		<p>PrimaryThree</p>
		<p>PrimaryFour</p>
		<p>PrimaryFive</p>
	</div>
	<div class="icons card">
		<h2>Icons</h2>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-alert-error.svg" alt="icon alert error" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-alert-info.svg" alt="icon alert info" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-alert-valid.svg" alt="icon alert valid" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-alert-warning.svg" alt="icon alert warning" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-menu.svg" alt="icon menu" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-add.svg" alt="icon add" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-close.svg" alt="icon close" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-search.svg" alt="icon search" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-phone.svg" alt="icon phone" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-mail.svg" alt="icon mail" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-cart.svg" alt="icon cart" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-map-marker.svg" alt="icon map maker" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-arrow-down.svg" alt="icon arrow down" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-arrow-up.svg" alt="icon arrow up" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-arrow-left.svg" alt="icon arrow left" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-arrow-right.svg" alt="icon arrow right" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-attachment.svg" alt="icon attachment" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-check.svg" alt="icon checkmark" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-download.svg" alt="icon download" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-upload.svg" alt="icon upload" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-folder.svg" alt="icon folder" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-image.svg" alt="icon image" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-media-play.svg" alt="icon media play" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-media-stop.svg" alt="icon media stop" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-men.svg" alt="icon men" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-women.svg" alt="icon women" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-bookmark.svg" alt="icon bookmark" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-favorite.svg" alt="icon favorite" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-delete.svg" alt="icon delete" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-like.svg" alt="icon like" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-unlike.svg" alt="icon unlike" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-share.svg" alt="icon share" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/social-facebook.svg" alt="icon social facebook" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/social-instagram.svg" alt="icon social instagram" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/social-linkedin.svg" alt="icon social linkedin" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/social-pinterest.svg" alt="icon social pinterest" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/social-twitter.svg" alt="icon social twitter" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/social-youtube.svg" alt="icon social youtube" />
		</div>
	</div>
</section>
<hr>