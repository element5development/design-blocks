<section class="icon-sliders">
	<header>
		<h2>Icon Slider</h2>
	</header>
	<div class="grid slider-parent">
		<!-- Parent display grid/flex throws off sizing -->
		<div class="card">
			<div class="icon-slider icon-slider-standard">
				<div class="icon">
					<img src="http://via.placeholder.com/250x150?text=ICON" alt="logo" />
				</div>
				<div class="icon">
					<img src="http://via.placeholder.com/250x150?text=ICON" alt="logo" />
				</div>
				<div class="icon">
					<img src="http://via.placeholder.com/250x150?text=ICON" alt="logo" />
				</div>
				<div class="icon">
					<img src="http://via.placeholder.com/250x150?text=ICON" alt="logo" />
				</div>
				<div class="icon">
					<img src="http://via.placeholder.com/250x150?text=ICON" alt="logo" />
				</div>
				<div class="icon">
					<img src="http://via.placeholder.com/250x150?text=ICON" alt="logo" />
				</div>
			</div>
		</div>
		<div class="card">
			<div class="icon-slider is-random">
				<div class="random-icon">
					<img class="swap">
					<img class="swap">
				</div>
				<div class="random-icon">
					<img class="swap">
					<img class="swap">
				</div>
				<div class="random-icon">
					<img class="swap">
					<img class="swap">
				</div>
			</div>
		</div>
	</div>
	<footer>
		<!-- INSTRUCTIONS AND REQUIRED VENDORS TO USE THESE FRAGMENTS -->
	</footer>
</section>
<hr>