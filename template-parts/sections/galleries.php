<section class="galleries">
	<header>
		<h2>Galleries</h2>
	</header>
	<div class="grid two-column">
		<div class="card">
			<div class="gallery">
				<a class="image" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg">
					<img src="https://via.placeholder.com/350x350?text=Thumbnaill%20One" alt="gallery image" />
				</a>
				<a class="image" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/code.jpg">
					<img src="https://via.placeholder.com/350x350?text=Thumbnaill%20Two" alt="gallery image" />
				</a>
				<a class="image" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg">
					<img src="https://via.placeholder.com/350x350?text=Thumbnaill%20Three" alt="gallery image" />
				</a>
				<a class="image" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/code.jpg">
					<img src="https://via.placeholder.com/350x350?text=Thumbnaill%20Four" alt="gallery image" />
				</a>
				<a class="image" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg">
					<img src="https://via.placeholder.com/350x350?text=Thumbnaill%20Five" alt="gallery image" />
				</a>
				<a class="image" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/code.jpg">
					<img src="https://via.placeholder.com/350x350?text=Thumbnaill%20Six" alt="gallery image" />
				</a>
			</div>
		</div>
		<div class="card">
			<div class="gallery is-masonry">
				<a class="masonry-image masonry-sizer" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg">
					<img src="https://via.placeholder.com/350x500?text=Thumbnaill%20One" alt="gallery image" />
				</a>
				<a class="masonry-image" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/code.jpg">
					<img src="https://via.placeholder.com/500x350?text=Thumbnaill%20Two" alt="gallery image" />
				</a>
				<a class="masonry-image" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg">
					<img src="https://via.placeholder.com/400x300?text=Thumbnaill%20Three" alt="gallery image" />
				</a>
				<a class="masonry-image" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/code.jpg">
					<img src="https://via.placeholder.com/325x400?text=Thumbnaill%20Four" alt="gallery image" />
				</a>
				<a class="masonry-image" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg">
					<img src="https://via.placeholder.com/475x475?text=Thumbnaill%20Five" alt="gallery image" />
				</a>
				<a class="masonry-image" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/code.jpg">
					<img src="https://via.placeholder.com/500x275?text=Thumbnaill%20Six" alt="gallery image" />
				</a>
			</div>
		</div>
	</div>
	</div>
	<footer>
		<!-- INSTRUCTIONS AND REQUIRED VENDORS TO USE THESE FRAGMENTS -->
	</footer>
</section>
<hr>