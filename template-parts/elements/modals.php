<section class="design-block">
	<h2>Modals/Dialogs</h2>
	<div class="modifer">
		<div class="modifer-header">
			<h3>Default</h3>
			<p class="subheader">a standard modal content section</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<button class="open-modal">Open Modal</button>
			<div class="modal is-hidden">
				<div class="dialog">
					<button class="close button is-icon">
						<svg>
							<use xlink:href="#close" />
						</svg>
					</button>
					<h2>Modal Card</h2>
					<p>Sed turpis mauris, luctus ac egestas pellentesque, maximus quis diam. Phasellus sodales fringilla leo, a efficitur velit
						elementum eget. Mauris velit risus, volutpat id fermentum vel, mollis ut felis. Proin consequat faucibus faucibus.
						Vivamus tempus metus at orci fermentum venenatis.</p>
					<div class="action-items">
						<button class="button is-primary is-ghost is-borderless">Agree</button>
						<button class="button is-primary is-ghost is-borderless">Disagree</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;button class="open-modal"&gt;Open Modal&lt;/button&gt;
						&lt;div class="modal is-hidden"&gt;
							&lt;div class="dialog"&gt;
								&lt;button class="close button is-icon"&gt;
									&lt;svg&gt;
										&lt;use xlink:href="#close" /&gt;
									&lt;/svg&gt;
								&lt;/button&gt;
								&lt;h2&gt;Modal Card&lt;/h2&gt;
								&lt;p&gt;Sed turpis mauris, luctus ac egestas pellentesque, maximus quis diam. Phasellus sodales fringilla leo, a efficitur velit
									elementum eget. Mauris velit risus, volutpat id fermentum vel, mollis ut felis. Proin consequat faucibus faucibus.
									Vivamus tempus metus at orci fermentum venenatis.&lt;/p&gt;
								&lt;div class="action-items"&gt;
									&lt;button class="button is-primary is-ghost is-borderless"&gt;Agree&lt;/button&gt;
									&lt;button class="button is-primary is-ghost is-borderless"&gt;Disagree&lt;/button&gt;
								&lt;/div&gt;
							&lt;/div&gt;
						&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/modals.postcss</li>
					<li>../master/modals.js</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.inline-modal</h3>
			<p class="subheader">modifier to apply modal styling effect when section in on screen</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="inline-modal" data-emergence="hidden">
				<button class="close button is-icon">
					<svg>
						<use xlink:href="#close" />
					</svg>
				</button>
				<h2>inline Modal</h2>
				<p>Sed turpis mauris, luctus ac egestas pellentesque, maximus quis diam. Phasellus sodales fringilla leo, a efficitur velit
					elementum eget. Mauris velit risus, volutpat id fermentum vel, mollis ut felis. Proin consequat faucibus faucibus. Vivamus
					tempus metus at orci fermentum venenatis.</p>
				<button class="button is-primary">Get it Today!</button>
			</div>
			<div class="overlay"></div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;/div&gt;
					&lt;div class="inline-modal" data-emergence="hidden"&gt;
						&lt;button class="close button is-icon"&gt;
							&lt;svg&gt;
								&lt;use xlink:href="#close" /&gt;
							&lt;/svg&gt;
						&lt;/button&gt;
						&lt;h2&gt;inline Modal&lt;/h2&gt;
						&lt;p&gt;Sed turpis mauris, luctus ac egestas pellentesque, maximus quis diam. Phasellus sodales fringilla leo, a efficitur velit
							elementum eget. Mauris velit risus, volutpat id fermentum vel, mollis ut felis. Proin consequat faucibus faucibus. Vivamus
							tempus metus at orci fermentum venenatis.&lt;/p&gt;
						&lt;button class="button is-primary"&gt;Get it Today!&lt;/button&gt;
					&lt;/div&gt;
					&lt;div class="overlay"&gt;&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/modals.postcss</li>
					<li>../master/modals.js</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
</section>