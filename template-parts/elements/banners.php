<section class="design-block">
	<h2>Banners</h2>
	<div class="modifer">
		<div class="modifer-header">
			<h3>Default</h3>
			<p class="subheader">a standard banner element</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="banner" style="background-image: url(https://images.unsplash.com/photo-1502911714542-002b74c313cf);">
				<div class="block">
					<h2>Banner Title</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices imperdiet. Mauris nec vestibulum
						eros. Nullam eu diam et arcu semper eleifend sit amet id purus.</p>
					<div class="buttons">
						<a class="button is-secondary" href="#">join now</a>
						<a class="button is-tertiary" href="#">learn more</a>
					</div>
				</div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="banner" style="background-image: url(https://images.unsplash.com/photo-1502911714542-002b74c313cf);"&gt;
						&lt;div class="block"&gt;
							&lt;h2&gt;Banner Title&lt;/h2&gt;
							&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis ultrices imperdiet. Mauris nec vestibulum
								eros. Nullam eu diam et arcu semper eleifend sit amet id purus.&lt;/p&gt;
							&lt;div class="buttons"&gt;
								&lt;a class="button is-secondary" href="#"&gt;join now&lt;/a&gt;
								&lt;a class="button is-tertiary" href="#"&gt;learn more&lt;/a&gt;
							&lt;/div&gt;
						&lt;/div&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/banners.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
</section>