<section class="design-block">
	<h2>Sliders/Carousel</h2>
	<div class="modifer">
		<div class="modifer-header">
			<h3>Default</h3>
			<p class="subheader">a standard slider content section</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="glide">
				<div class="glide__track" data-glide-el="track">
					<ul class="glide__slides">
						<li class="glide__slide">
							<div class="block">
								<h3>1</h3>
							</div>
						</li>
						<li class="glide__slide">
							<div class="block">
								<h3>2</h3>
							</div>
						</li>
						<li class="glide__slide">
							<div class="block">
								<h3>3</h3>
							</div>
						</li>
					</ul>
				</div>
				<div class="glide__arrows" data-glide-el="controls">
					<button class="glide__arrow glide__arrow--prev" data-glide-dir="<">
						<svg>
							<use xlink:href="#arrowhead-left" />
						</svg>
					</button>
					<button class="glide__arrow glide__arrow--next" data-glide-dir=">">
						<svg>
							<use xlink:href="#arrowhead-right" />
						</svg>
					</button>
				</div>
				<div class="glide__bullets" data-glide-el="controls[nav]">
					<button class="glide__bullet" data-glide-dir="=0"></button>
					<button class="glide__bullet" data-glide-dir="=1"></button>
					<button class="glide__bullet" data-glide-dir="=2"></button>
				</div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="glide"&gt;
						&lt;div class="glide__track" data-glide-el="track"&gt;
							&lt;ul class="glide__slides"&gt;
								&lt;li class="glide__slide"&gt;
									&lt;div class="block"&gt;
										&lt;h3&gt;1&lt;/h3&gt;
									&lt;/div&gt;
								&lt;/li&gt;
								&lt;li class="glide__slide"&gt;
									&lt;div class="block"&gt;
										&lt;h3&gt;2&lt;/h3&gt;
									&lt;/div&gt;
								&lt;/li&gt;
								&lt;li class="glide__slide"&gt;
									&lt;div class="block"&gt;
										&lt;h3&gt;3&lt;/h3&gt;
									&lt;/div&gt;
								&lt;/li&gt;
							&lt;/ul&gt;
						&lt;/div&gt;
						&lt;div class="glide__arrows" data-glide-el="controls"&gt;
							&lt;button class="glide__arrow glide__arrow--prev" data-glide-dir="&lt;"&gt;
								&lt;svg&gt;
									&lt;use xlink:href="#arrowhead-left" /&gt;
								&lt;/svg&gt;
							&lt;/button&gt;
							&lt;button class="glide__arrow glide__arrow--next" data-glide-dir="&gt;"&gt;
								&lt;svg&gt;
									&lt;use xlink:href="#arrowhead-right" /&gt;
								&lt;/svg&gt;
							&lt;/button&gt;
						&lt;/div&gt;
						&lt;div class="glide__bullets" data-glide-el="controls[nav]"&gt;
							&lt;button class="glide__bullet" data-glide-dir="=0"&gt;&lt;/button&gt;
							&lt;button class="glide__bullet" data-glide-dir="=1"&gt;&lt;/button&gt;
							&lt;button class="glide__bullet" data-glide-dir="=2"&gt;&lt;/button&gt;
						&lt;/div&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../vendors/glidecore.css</li>
					<li>../fragments/glidetheme.css</li>
					<li>../vendors/sliders.postcss</li>
					<li>../vendors/glide.js</li>
					<li>../master/sliders.js</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.icon-glide</h3>
			<p class="subheader">modifier used when sldier consists of a series of same size logos</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="icon-glide">
				<div class="glide__track" data-glide-el="track">
					<ul class="glide__slides">
						<li class="glide__slide">
							<div class="block">
								<img src="http://via.placeholder.com/350x200?text=LOGO" alt="logo" />
							</div>
						</li>
						<li class="glide__slide">
							<div class="block">
								<img src="http://via.placeholder.com/350x200?text=LOGO" alt="logo" />
							</div>
						</li>
						<li class="glide__slide">
							<div class="block">
								<img src="http://via.placeholder.com/350x200?text=LOGO" alt="logo" />
							</div>
						</li>
						<li class="glide__slide">
							<div class="block">
								<img src="http://via.placeholder.com/350x200?text=LOGO" alt="logo" />
							</div>
						</li>
						<li class="glide__slide">
							<div class="block">
								<img src="http://via.placeholder.com/350x200?text=LOGO" alt="logo" />
							</div>
						</li>
						<li class="glide__slide">
							<div class="block">
								<img src="http://via.placeholder.com/350x200?text=LOGO" alt="logo" />
							</div>
						</li>
						<li class="glide__slide">
							<div class="block">
								<img src="http://via.placeholder.com/350x200?text=LOGO" alt="logo" />
							</div>
						</li>
						<li class="glide__slide">
							<div class="block">
								<img src="http://via.placeholder.com/350x200?text=LOGO" alt="logo" />
							</div>
						</li>
						<li class="glide__slide">
							<div class="block">
								<img src="http://via.placeholder.com/350x200?text=LOGO" alt="logo" />
							</div>
						</li>
						<li class="glide__slide">
							<div class="block">
								<img src="http://via.placeholder.com/350x200?text=LOGO" alt="logo" />
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="icon-glide"&gt;
						&lt;div class="glide__track" data-glide-el="track"&gt;
							&lt;ul class="glide__slides"&gt;
								&lt;li class="glide__slide"&gt;
									&lt;div class="block"&gt;
										&lt;img src="http://via.placeholder.com/350x200?text=LOGO" alt="logo" /&gt;
									&lt;/div&gt;
								&lt;/li&gt;
								&lt;li class="glide__slide"&gt;
									&lt;div class="block"&gt;
										&lt;img src="http://via.placeholder.com/350x200?text=LOGO" alt="logo" /&gt;
									&lt;/div&gt;
								&lt;/li&gt;
								&lt;li class="glide__slide"&gt;
									&lt;div class="block"&gt;
										&lt;img src="http://via.placeholder.com/350x200?text=LOGO" alt="logo" /&gt;
									&lt;/div&gt;
								&lt;/li&gt;
								&lt;li class="glide__slide"&gt;
									&lt;div class="block"&gt;
										&lt;img src="http://via.placeholder.com/350x200?text=LOGO" alt="logo" /&gt;
									&lt;/div&gt;
								&lt;/li&gt;
								&lt;li class="glide__slide"&gt;
									&lt;div class="block"&gt;
										&lt;img src="http://via.placeholder.com/350x200?text=LOGO" alt="logo" /&gt;
									&lt;/div&gt;
								&lt;/li&gt;
								&lt;li class="glide__slide"&gt;
									&lt;div class="block"&gt;
										&lt;img src="http://via.placeholder.com/350x200?text=LOGO" alt="logo" /&gt;
									&lt;/div&gt;
								&lt;/li&gt;
								&lt;li class="glide__slide"&gt;
									&lt;div class="block"&gt;
										&lt;img src="http://via.placeholder.com/350x200?text=LOGO" alt="logo" /&gt;
									&lt;/div&gt;
								&lt;/li&gt;
								&lt;li class="glide__slide"&gt;
									&lt;div class="block"&gt;
										&lt;img src="http://via.placeholder.com/350x200?text=LOGO" alt="logo" /&gt;
									&lt;/div&gt;
								&lt;/li&gt;
								&lt;li class="glide__slide"&gt;
									&lt;div class="block"&gt;
										&lt;img src="http://via.placeholder.com/350x200?text=LOGO" alt="logo" /&gt;
									&lt;/div&gt;
								&lt;/li&gt;
								&lt;li class="glide__slide"&gt;
									&lt;div class="block"&gt;
										&lt;img src="http://via.placeholder.com/350x200?text=LOGO" alt="logo" /&gt;
									&lt;/div&gt;
								&lt;/li&gt;
							&lt;/ul&gt;
						&lt;/div&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../vendors/glidecore.css</li>
					<li>../fragments/glidetheme.css</li>
					<li>../vendors/sliders.postcss</li>
					<li>../vendors/glide.js</li>
					<li>../master/sliders.js</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
</section>