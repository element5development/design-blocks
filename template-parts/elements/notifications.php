<section class="design-block">
	<h2>Notifications</h2>
	<div class="modifer">
		<div class="modifer-header">
			<h3>Default</h3>
			<p class="subheader">a standard notification element</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="notification">
				<button class="close button is-icon is-transparent is-small">
					<svg>
						<use xlink:href="#close" />
					</svg>
				</button>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut mollis ipsum, a facilisis nulla. Curabitur faucibus
					ultricies nibh, non aliquam lectus vulputate a. Aliquam lobortis purus id feugiat semper. Nullam ut elit non neque rhoncus
					elementum. </p>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="notification"&gt;
						&lt;button class="button is-icon is-transparent is-small"&gt;
							&lt;svg&gt;
								&lt;use xlink:href="#close" /&gt;
							&lt;/svg&gt;
						&lt;/button&gt;
						&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut mollis ipsum, a facilisis nulla. Curabitur faucibus
							ultricies nibh, non aliquam lectus vulputate a. Aliquam lobortis purus id feugiat semper. Nullam ut elit non neque rhoncus
							elementum. &lt;/p&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../elements/notifications.postcss</li>
					<li>../master/notifications.js</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.has-icon</h3>
			<p class="subheader">modifer to display an icon before the message</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="notification has-icon">
				<button class="close button is-icon is-transparent is-small">
					<svg>
						<use xlink:href="#close" />
					</svg>
				</button>
				<svg>
					<use xlink:href="#rocket" />
				</svg>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut mollis ipsum, a facilisis nulla. Curabitur faucibus
					ultricies nibh, non aliquam lectus vulputate a. Aliquam lobortis purus id feugiat semper. Nullam ut elit non neque rhoncus
					elementum. </p>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="notification has-icon"&gt;
						&lt;button class="button is-icon is-transparent is-small"&gt;
							&lt;svg&gt;
								&lt;use xlink:href="#close" /&gt;
							&lt;/svg&gt;
						&lt;/button&gt;
						&lt;svg&gt;
							&lt;use xlink:href="#rocket" /&gt;
						&lt;/svg&gt;
						&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut mollis ipsum, a facilisis nulla. Curabitur faucibus
							ultricies nibh, non aliquam lectus vulputate a. Aliquam lobortis purus id feugiat semper. Nullam ut elit non neque rhoncus
							elementum. &lt;/p&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../elements/notifications.postcss</li>
					<li>../master/notifications.js</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.is-[color]</h3>
			<p class="subheader">modifier to adjust the notification's color</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="notification is-primary">
				<button class="close button is-icon is-transparent is-small">
					<svg>
						<use xlink:href="#close" />
					</svg>
				</button>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut mollis ipsum, a facilisis nulla.</p>
			</div>
			<div class="notification is-secondary">
				<button class="close button is-icon is-transparent is-small">
					<svg>
						<use xlink:href="#close" />
					</svg>
				</button>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut mollis ipsum, a facilisis nulla.</p>
			</div>
			<div class="notification is-tertiary">
				<button class="close button is-icon is-transparent is-small">
					<svg>
						<use xlink:href="#close" />
					</svg>
				</button>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut mollis ipsum, a facilisis nulla.</p>
			</div>
			<div class="notification is-negative">
				<button class="close button is-icon is-transparent is-small">
					<svg>
						<use xlink:href="#close" />
					</svg>
				</button>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut mollis ipsum, a facilisis nulla.</p>
			</div>
			<div class="notification is-warning">
				<button class="close button is-icon is-transparent is-small">
					<svg>
						<use xlink:href="#close" />
					</svg>
				</button>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut mollis ipsum, a facilisis nulla.</p>
			</div>
			<div class="notification is-positive">
				<button class="close button is-icon is-transparent is-small">
					<svg>
						<use xlink:href="#close" />
					</svg>
				</button>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut mollis ipsum, a facilisis nulla.</p>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="notification is-primary"&gt;
						&lt;button class="button is-icon is-transparent is-small"&gt;
							&lt;svg&gt;
								&lt;use xlink:href="#close" /&gt;
							&lt;/svg&gt;
						&lt;/button&gt;
						&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut mollis ipsum, a facilisis nulla.&lt;/p&gt;
					&lt;/div&gt;
					&lt;div class="notification is-secondary"&gt;
						&lt;button class="button is-icon is-transparent is-small"&gt;
							&lt;svg&gt;
								&lt;use xlink:href="#close" /&gt;
							&lt;/svg&gt;
						&lt;/button&gt;
						&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut mollis ipsum, a facilisis nulla.&lt;/p&gt;
					&lt;/div&gt;
					&lt;div class="notification is-tertiary"&gt;
						&lt;button class="button is-icon is-transparent is-small"&gt;
							&lt;svg&gt;
								&lt;use xlink:href="#close" /&gt;
							&lt;/svg&gt;
						&lt;/button&gt;
						&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut mollis ipsum, a facilisis nulla.&lt;/p&gt;
					&lt;/div&gt;
					&lt;div class="notification is-negative"&gt;
						&lt;button class="button is-icon is-transparent is-small"&gt;
							&lt;svg&gt;
								&lt;use xlink:href="#close" /&gt;
							&lt;/svg&gt;
						&lt;/button&gt;
						&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut mollis ipsum, a facilisis nulla.&lt;/p&gt;
					&lt;/div&gt;
					&lt;div class="notification is-warning"&gt;
						&lt;button class="button is-icon is-transparent is-small"&gt;
							&lt;svg&gt;
								&lt;use xlink:href="#close" /&gt;
							&lt;/svg&gt;
						&lt;/button&gt;
						&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut mollis ipsum, a facilisis nulla.&lt;/p&gt;
					&lt;/div&gt;
					&lt;div class="notification is-positive"&gt;
						&lt;button class="button is-icon is-transparent is-small"&gt;
							&lt;svg&gt;
								&lt;use xlink:href="#close" /&gt;
							&lt;/svg&gt;
						&lt;/button&gt;
						&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut mollis ipsum, a facilisis nulla.&lt;/p&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../elements/notifications.postcss</li>
					<li>../master/notifications.js</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
</section>