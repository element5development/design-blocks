<section class="design-block">
	<h2>Cards</h2>
	<div class="modifer">
		<div class="modifer-header">
			<h3>Default</h3>
			<p class="subheader">a standard card element</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="card">
				<h3 class="has-subheader is-padded">Lorem ipsum dolor</h3>
				<p class="subheader is-padded">Nam feugiat venenatis metus at bibendum. Fusce semper sollicitudin urna, ut pellentesque risus fringilla quis. Vivamus
					sollicitudin dolor vel maximus interdum.</p>
				<div class="buttons is-padded">
					<a class="button is-primary" href="#">primary</a>
				</div>
			</div>
			<div class="card">
				<h3 class="is-padded">Lorem ipsum dolor</h3>
				<ul>
					<li>Donec ultricies</li>
					<li>Curabitur id facilisis massa</li>
					<li>Aliquam non pulvinar</li>
					<li>Suspendisse imperdiet</li>
				</ul>
				<a class="button is-fluid" href="#">primary</a>
			</div>
			<div class="card">
				<h3 class="has-subheader is-padded">Lorem ipsum dolor</h3>
				<p class="subheader is-padded">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				<p class="is-padded">Nam feugiat venenatis metus at bibendum. Fusce semper sollicitudin urna, ut pellentesque risus fringilla quis. Vivamus
					sollicitudin dolor vel maximus interdum.</p>
				<div class="buttons are-grouped is-padded">
					<a class="button is-primary" href="#">call</a>
					<a class="button is-secondary" href="#">email</a>
					<a class="button is-tertiary" href="#">fax</a>
				</div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/tabs.postcss</li>
					<li>../master/tabs.js</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>Default</h3>
			<p class="subheader">a standard card element with an icon</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="card">
				<div class="featured-icon">
					<svg>
						<use xlink:href="#rocket" />
					</svg>
				</div>
				<h3 class="is-padded">Lorem ipsum dolor</h3>
				<p class="is-padded">Nam feugiat venenatis metus at bibendum. Fusce semper sollicitudin urna, ut pellentesque risus fringilla quis. Vivamus
					sollicitudin dolor vel maximus interdum.</p>
				<a class="button is-primary is-fluid" href="#">primary</a>
			</div>
			<div class="card">
				<div class="featured-icon">
					<svg>
						<use xlink:href="#rocket" />
					</svg>
				</div>
				<h3 class="has-subheader is-padded">Lorem ipsum dolor</h3>
				<p class="subheader is-padded">Donec cursus nibh vel velit finibus, at pharetra enim sodales.</p>
				<p class="is-padded">Nam feugiat venenatis metus at bibendum. Fusce semper sollicitudin urna, ut pellentesque risus fringilla quis. Vivamus
					sollicitudin dolor vel maximus interdum.</p>
				<div class="buttons is-padded">
					<a class="button is-primary" href="#">primary</a>
					<a class="button is-secondary" href="#">primary</a>
				</div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/tabs.postcss</li>
					<li>../master/tabs.js</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>Default</h3>
			<p class="subheader">a standard card element with images and featured iamges</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="card">
				<img src="https://images.unsplash.com/photo-1502911714542-002b74c313cf" alt="image" />
				<h3 class="has-subheader is-padded">Lorem ipsum dolor</h3>
				<p class="subheader is-padded">Nam feugiat venenatis metus at bibendum. Fusce semper sollicitudin urna, ut pellentesque risus fringilla quis. Vivamus
					sollicitudin dolor vel maximus interdum.</p>
				<div class="buttons is-padded">
					<a class="button is-primary is-fluid" href="#">primary</a>
				</div>
			</div>
			<div class="card">
				<div class="featured-image">
					<img src="https://images.unsplash.com/photo-1502911714542-002b74c313cf" alt="image" />
					<div class="block">
						<h3 class="has-subheader">Lorem ipsum dolor</h3>
						<p class="subheader">Aliquam dolor lacus, porttitor vel consequat a, tempor vitae purus.</p>
					</div>
					<div class="chip is-icon on-corner">
						<svg>
							<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#atom"></use>
						</svg>
					</div>
				</div>
				<p class="is-padded">Nam feugiat venenatis metus at bibendum. Fusce semper sollicitudin urna, ut pellentesque risus fringilla quis. Vivamus
					sollicitudin dolor vel maximus interdum.</p>
				<div class="buttons is-padded">
					<a class="button is-primary" href="#">primary</a>
				</div>
			</div>
			<div class="card">
				<div class="featured-image has-overlay">
					<img src="https://images.unsplash.com/photo-1502911714542-002b74c313cf" alt="image" />
					<div class="block">
						<h3 class="has-subheader">Lorem ipsum dolor</h3>
						<p class="subheader">Aliquam dolor lacus, porttitor vel consequat a, tempor vitae purus.</p>
					</div>
					<div class="chips are-stacked">
						<div class="chip">Category</div>
						<div class="chip">Multiple</div>
					</div>
				</div>
				<p class="is-padded">Nam feugiat venenatis metus at bibendum. Fusce semper sollicitudin urna, ut pellentesque risus fringilla quis. Vivamus
					sollicitudin dolor vel maximus interdum.</p>
				<div class="buttons is-padded">
					<a class="button is-primary" href="#">primary</a>
					<a class="button is-secondary" href="#">primary</a>
				</div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/tabs.postcss</li>
					<li>../master/tabs.js</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>Default</h3>
			<p class="subheader">a standard card element with meta information</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="card">
				<h3 class="has-subheader is-padded">Lorem ipsum dolor</h3>
				<p class="subheader is-padded">Nam feugiat venenatis metus at bibendum.</p>
				<p class="is-padded">Curabitur ut mi odio. Maecenas efficitur tortor dolor. Quisque in iaculis turpis, imperdiet lobortis nulla. Donec eget
					lectus bibendum, euismod justo id, facilisis eros.</p>
				<div class="meta is-padded">
					<div class="headshot">
						<img src="http://192.168.33.10/design-system/wp-content/uploads/2018/04/Caveman.png" alt="headshot" />
					</div>
					<div class="info">
						<p>By: John Doe
							<br/>October 31, 2018</p>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="featured-icon">
					<svg>
						<use xlink:href="#rocket" />
					</svg>
				</div>
				<h3 class="is-padded">Lorem ipsum dolor</h3>
				<div class="meta is-padded">
					<div class="headshot">
						<img src="http://192.168.33.10/design-system/wp-content/uploads/2018/04/Caveman.png" alt="headshot" />
					</div>
					<div class="info">
						<p>By: John Doe
							<br/>October 31, 2018</p>
					</div>
				</div>
				<p class="is-padded">Nam feugiat venenatis metus at bibendum. Fusce semper sollicitudin urna, ut pellentesque risus fringilla quis. Vivamus
					sollicitudin dolor vel maximus interdum.</p>
				<a class="button is-tertiary is-fluid" href="#">primary</a>
			</div>
			<div class="card">
				<div class="featured-image has-overlay">
					<img src="https://images.unsplash.com/photo-1502911714542-002b74c313cf" alt="image" />
					<div class="block">
						<h3 class="has-subheader">Lorem ipsum dolor</h3>
						<p class="subheader">Aliquam dolor lacus, porttitor vel consequat a, tempor vitae purus.</p>
					</div>
					<div class="chips are-stacked">
						<div class="chip">Category</div>
						<div class="chip">Multiple</div>
					</div>
				</div>
				<div class="meta is-padded">
					<div class="headshot">
						<img src="http://192.168.33.10/design-system/wp-content/uploads/2018/04/Caveman.png" alt="headshot" />
					</div>
					<div class="info">
						<p>By: John Doe
							<br/>October 31, 2018</p>
					</div>
				</div>
				<p class="is-padded">Nam feugiat venenatis metus at bibendum. Fusce semper sollicitudin urna, ut pellentesque risus fringilla quis. Vivamus
					sollicitudin dolor vel maximus interdum.</p>
				<div class="buttons is-padded">
					<a class="button has-icon is-primary" href="#">
						primary
						<svg>
							<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrowhead-right"></use>
						</svg>
					</a>
				</div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/tabs.postcss</li>
					<li>../master/tabs.js</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
</section>