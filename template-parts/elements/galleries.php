<section class="design-block">
	<h2>GALLERIES</h2>
	<div class="modifer">
		<div class="modifer-header">
			<h3>Default</h3>
			<p class="subheader">a standard gallery element</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="gallery">
				<a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/code.jpg">
					<img src="http://via.placeholder.com/200x200?text=thumb" alt="image thumbnail" />
				</a>
				<a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg">
					<img src="http://via.placeholder.com/200x200?text=thumb" alt="image thumbnail" />
				</a>
				<a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/code.jpg">
					<img src="http://via.placeholder.com/200x200?text=thumb" alt="image thumbnail" />
				</a>
				<a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg">
					<img src="http://via.placeholder.com/200x200?text=thumb" alt="image thumbnail" />
				</a>
				<a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/code.jpg">
					<img src="http://via.placeholder.com/200x200?text=thumb" alt="image thumbnail" />
				</a>
				<a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg">
					<img src="http://via.placeholder.com/200x200?text=thumb" alt="image thumbnail" />
				</a>
				<a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/code.jpg">
					<img src="http://via.placeholder.com/200x200?text=thumb" alt="image thumbnail" />
				</a>
				<a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg">
					<img src="http://via.placeholder.com/200x200?text=thumb" alt="image thumbnail" />
				</a>
				<a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg">
					<img src="http://via.placeholder.com/200x200?text=thumb" alt="image thumbnail" />
				</a>
				<a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg">
					<img src="http://via.placeholder.com/200x200?text=thumb" alt="image thumbnail" />
				</a>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="gallery"&gt;
						&lt;a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/code.jpg"&gt;
							&lt;img src="http://via.placeholder.com/200x200?text=thumb" alt="image thumbnail" /&gt;
						&lt;/a&gt;
						&lt;a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg"&gt;
							&lt;img src="http://via.placeholder.com/200x200?text=thumb" alt="image thumbnail" /&gt;
						&lt;/a&gt;
						&lt;a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/code.jpg"&gt;
							&lt;img src="http://via.placeholder.com/200x200?text=thumb" alt="image thumbnail" /&gt;
						&lt;/a&gt;
						&lt;a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg"&gt;
							&lt;img src="http://via.placeholder.com/200x200?text=thumb" alt="image thumbnail" /&gt;
						&lt;/a&gt;
						&lt;a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/code.jpg"&gt;
							&lt;img src="http://via.placeholder.com/200x200?text=thumb" alt="image thumbnail" /&gt;
						&lt;/a&gt;
						&lt;a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg"&gt;
							&lt;img src="http://via.placeholder.com/200x200?text=thumb" alt="image thumbnail" /&gt;
						&lt;/a&gt;
						&lt;a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/code.jpg"&gt;
							&lt;img src="http://via.placeholder.com/200x200?text=thumb" alt="image thumbnail" /&gt;
						&lt;/a&gt;
						&lt;a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg"&gt;
							&lt;img src="http://via.placeholder.com/200x200?text=thumb" alt="image thumbnail" /&gt;
						&lt;/a&gt;
						&lt;a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg"&gt;
							&lt;img src="http://via.placeholder.com/200x200?text=thumb" alt="image thumbnail" /&gt;
						&lt;/a&gt;
						&lt;a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg"&gt;
							&lt;img src="http://via.placeholder.com/200x200?text=thumb" alt="image thumbnail" /&gt;
						&lt;/a&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../vendors/featherlight.css</li>
					<li>../vendors/featherlightgallery.css</li>
					<li>../fragments/galleries.postcss</li>
					<li>../vendors/featherlight.js</li>
					<li>../vendors/featherlightgallery.js</li>
					<li>../master/galleries.js</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.is-masonry</h3>
			<p class="subheader">modifier to aply masonry styles</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="gallery is-masonry">
				<a class="item sizer" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/code.jpg">
					<img src="http://via.placeholder.com/200x400?text=thumb" alt="image thumbnail" />
				</a>
				<a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg">
					<img src="http://via.placeholder.com/200x100?text=thumb" alt="image thumbnail" />
				</a>
				<a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/code.jpg">
					<img src="http://via.placeholder.com/200x250?text=thumb" alt="image thumbnail" />
				</a>
				<a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg">
					<img src="http://via.placeholder.com/200x200?text=thumb" alt="image thumbnail" />
				</a>
				<a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/code.jpg">
					<img src="http://via.placeholder.com/200x400?text=thumb" alt="image thumbnail" />
				</a>
				<a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg">
					<img src="http://via.placeholder.com/200x600?text=thumb" alt="image thumbnail" />
				</a>
				<a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/code.jpg">
					<img src="http://via.placeholder.com/200x150?text=thumb" alt="image thumbnail" />
				</a>
				<a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg">
					<img src="http://via.placeholder.com/200x250?text=thumb" alt="image thumbnail" />
				</a>
				<a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg">
					<img src="http://via.placeholder.com/200x350?text=thumb" alt="image thumbnail" />
				</a>
				<a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg">
					<img src="http://via.placeholder.com/200x300?text=thumb" alt="image thumbnail" />
				</a>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="gallery is-masonry"&gt;
						&lt;a class="item sizer" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/code.jpg"&gt;
							&lt;img src="http://via.placeholder.com/200x400?text=thumb" alt="image thumbnail" /&gt;
						&lt;/a&gt;
						&lt;a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg"&gt;
							&lt;img src="http://via.placeholder.com/200x100?text=thumb" alt="image thumbnail" /&gt;
						&lt;/a&gt;
						&lt;a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/code.jpg"&gt;
							&lt;img src="http://via.placeholder.com/200x250?text=thumb" alt="image thumbnail" /&gt;
						&lt;/a&gt;
						&lt;a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg"&gt;
							&lt;img src="http://via.placeholder.com/200x200?text=thumb" alt="image thumbnail" /&gt;
						&lt;/a&gt;
						&lt;a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/code.jpg"&gt;
							&lt;img src="http://via.placeholder.com/200x400?text=thumb" alt="image thumbnail" /&gt;
						&lt;/a&gt;
						&lt;a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg"&gt;
							&lt;img src="http://via.placeholder.com/200x600?text=thumb" alt="image thumbnail" /&gt;
						&lt;/a&gt;
						&lt;a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/code.jpg"&gt;
							&lt;img src="http://via.placeholder.com/200x150?text=thumb" alt="image thumbnail" /&gt;
						&lt;/a&gt;
						&lt;a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg"&gt;
							&lt;img src="http://via.placeholder.com/200x250?text=thumb" alt="image thumbnail" /&gt;
						&lt;/a&gt;
						&lt;a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg"&gt;
							&lt;img src="http://via.placeholder.com/200x350?text=thumb" alt="image thumbnail" /&gt;
						&lt;/a&gt;
						&lt;a class="item" href="http://192.168.33.10/design-system/wp-content/uploads/2018/03/desk.jpg"&gt;
							&lt;img src="http://via.placeholder.com/200x300?text=thumb" alt="image thumbnail" /&gt;
						&lt;/a&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../vendors/featherlight.css</li>
					<li>../vendors/featherlightgallery.css</li>
					<li>../fragments/galleries.postcss</li>
					<li>../vendors/featherlight.js</li>
					<li>../vendors/featherlightgallery.js</li>
					<li>../vendors/masonry.pkgd.js</li>
					<li>../master/galleries.js</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
</section>