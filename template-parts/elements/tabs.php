<section class="design-block">
	<h2>Tabs</h2>
	<div class="modifer">
		<div class="modifer-header">
			<h3>Default</h3>
			<p class="subheader">a standard tab content element</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="tabs">
				<nav>
					<button class="is-active is-ghost">Tab One</button>
					<button class="is-ghost">
						Tab Two
					</button>
					<button class="is-ghost">Tab Three</button>
				</nav>
				<div class="tab-contents">
					<div class="tab is-active">
						<h3>Tab One</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse luctus nunc non ante sollicitudin, et molestie
							est rhoncus. Curabitur diam lectus, viverra hendrerit nulla non, gravida varius ex. Suspendisse sodales nibh sit amet
							maximus vulputate. In vulputate turpis vel sem sagittis accumsan. Vivamus volutpat, neque a tempus fermentum, nulla
							purus accumsan sem, et convallis lacus turpis ac massa. Suspendisse potenti.</p>
					</div>
					<div class="tab">
						<h3>Tab Two</h3>
						<p>Sed a tortor risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed et sem at nisi convallis molestie
							id id eros. Cras accumsan tincidunt augue, sit amet dapibus nibh lobortis vitae. Vestibulum at eros dapibus, lacinia
							enim ac, feugiat sapien. Donec consectetur leo ligula.</p>
					</div>
					<div class="tab">
						<h3>Tab Three</h3>
						<p>Aliquam congue feugiat urna non pellentesque. In vehicula enim ut risus ornare, a commodo diam egestas. Duis sagittis
							elit sapien, non fringilla purus iaculis id. Duis id nisl laoreet, egestas lorem id, rutrum ex. Curabitur vehicula
							pulvinar nibh. Duis fermentum orci neque, id mattis purus commodo dictum. Fusce malesuada nunc neque, sagittis auctor
							turpis sollicitudin a. Morbi ac rutrum turpis.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="tabs"&gt;
						&lt;nav&gt;
							&lt;button class="is-active is-ghost"&gt;Tab One&lt;/button&gt;
							&lt;button class="is-ghost"&gt;Tab Two&lt;/button&gt;
							&lt;button class="is-ghost"&gt;Tab Three&lt;/button&gt;
						&lt;/nav&gt;
						&lt;div class="tab-contents"&gt;
							&lt;div class="tab is-active"&gt;
								&lt;h3&gt;Tab One&lt;/h3&gt;
								&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse luctus nunc non ante sollicitudin, et molestie
									est rhoncus. Curabitur diam lectus, viverra hendrerit nulla non, gravida varius ex. Suspendisse sodales nibh sit amet
									maximus vulputate. In vulputate turpis vel sem sagittis accumsan. Vivamus volutpat, neque a tempus fermentum, nulla
									purus accumsan sem, et convallis lacus turpis ac massa. Suspendisse potenti.&lt;/p&gt;
							&lt;/div&gt;
							&lt;div class="tab"&gt;
								&lt;h3&gt;Tab Two&lt;/h3&gt;
								&lt;p&gt;Sed a tortor risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed et sem at nisi convallis molestie
									id id eros. Cras accumsan tincidunt augue, sit amet dapibus nibh lobortis vitae. Vestibulum at eros dapibus, lacinia
									enim ac, feugiat sapien. Donec consectetur leo ligula.&lt;/p&gt;
							&lt;/div&gt;
							&lt;div class="tab"&gt;
								&lt;h3&gt;Tab Three&lt;/h3&gt;
								&lt;p&gt;Aliquam congue feugiat urna non pellentesque. In vehicula enim ut risus ornare, a commodo diam egestas. Duis sagittis
									elit sapien, non fringilla purus iaculis id. Duis id nisl laoreet, egestas lorem id, rutrum ex. Curabitur vehicula
									pulvinar nibh. Duis fermentum orci neque, id mattis purus commodo dictum. Fusce malesuada nunc neque, sagittis auctor
									turpis sollicitudin a. Morbi ac rutrum turpis.&lt;/p&gt;
							&lt;/div&gt;
						&lt;/div&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/tabs.postcss</li>
					<li>../master/tabs.js</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.are-stacked</h3>
			<p class="subheader">modifier to make navigation stacked on the left side</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="tabs are-stacked">
				<nav>
					<button class="is-active is-ghost">Tab One</button>
					<button class="is-ghost">Tab Two</button>
					<button class="is-ghost">Tab Three</button>
				</nav>
				<div class="tab-contents">
					<div class="tab is-active">
						<h3>Tab One</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse luctus nunc non ante sollicitudin, et molestie
							est rhoncus. Curabitur diam lectus, viverra hendrerit nulla non, gravida varius ex. Suspendisse sodales nibh sit amet
							maximus vulputate. In vulputate turpis vel sem sagittis accumsan. Vivamus volutpat, neque a tempus fermentum, nulla
							purus accumsan sem, et convallis lacus turpis ac massa. Suspendisse potenti.</p>
					</div>
					<div class="tab">
						<h3>Tab Two</h3>
						<p>Sed a tortor risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed et sem at nisi convallis molestie
							id id eros. Cras accumsan tincidunt augue, sit amet dapibus nibh lobortis vitae. Vestibulum at eros dapibus, lacinia
							enim ac, feugiat sapien. Donec consectetur leo ligula.</p>
					</div>
					<div class="tab">
						<h3>Tab Three</h3>
						<p>Aliquam congue feugiat urna non pellentesque. In vehicula enim ut risus ornare, a commodo diam egestas. Duis sagittis
							elit sapien, non fringilla purus iaculis id. Duis id nisl laoreet, egestas lorem id, rutrum ex. Curabitur vehicula
							pulvinar nibh. Duis fermentum orci neque, id mattis purus commodo dictum. Fusce malesuada nunc neque, sagittis auctor
							turpis sollicitudin a. Morbi ac rutrum turpis.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="tabs are-stacked"&gt;
						&lt;nav&gt;
							&lt;button class="is-active is-ghost"&gt;Tab One&lt;/button&gt;
							&lt;button class="is-ghost"&gt;Tab Two&lt;/button&gt;
							&lt;button class="is-ghost"&gt;Tab Three&lt;/button&gt;
						&lt;/nav&gt;
						&lt;div class="tab-contents"&gt;
							&lt;div class="tab is-active"&gt;
								&lt;h3&gt;Tab One&lt;/h3&gt;
								&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse luctus nunc non ante sollicitudin, et molestie
									est rhoncus. Curabitur diam lectus, viverra hendrerit nulla non, gravida varius ex. Suspendisse sodales nibh sit amet
									maximus vulputate. In vulputate turpis vel sem sagittis accumsan. Vivamus volutpat, neque a tempus fermentum, nulla
									purus accumsan sem, et convallis lacus turpis ac massa. Suspendisse potenti.&lt;/p&gt;
							&lt;/div&gt;
							&lt;div class="tab"&gt;
								&lt;h3&gt;Tab Two&lt;/h3&gt;
								&lt;p&gt;Sed a tortor risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed et sem at nisi convallis molestie
									id id eros. Cras accumsan tincidunt augue, sit amet dapibus nibh lobortis vitae. Vestibulum at eros dapibus, lacinia
									enim ac, feugiat sapien. Donec consectetur leo ligula.&lt;/p&gt;
							&lt;/div&gt;
							&lt;div class="tab"&gt;
								&lt;h3&gt;Tab Three&lt;/h3&gt;
								&lt;p&gt;Aliquam congue feugiat urna non pellentesque. In vehicula enim ut risus ornare, a commodo diam egestas. Duis sagittis
									elit sapien, non fringilla purus iaculis id. Duis id nisl laoreet, egestas lorem id, rutrum ex. Curabitur vehicula
									pulvinar nibh. Duis fermentum orci neque, id mattis purus commodo dictum. Fusce malesuada nunc neque, sagittis auctor
									turpis sollicitudin a. Morbi ac rutrum turpis.&lt;/p&gt;
							&lt;/div&gt;
						&lt;/div&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/tabs.postcss</li>
					<li>../master/tabs.js</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
</section>