<section class="design-block">
	<h2>Accordion</h2>
	<div class="modifer">
		<div class="modifer-header">
			<h3>Default</h3>
			<p class="subheader">a standard accordion content element</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="accordion">
				<button class="accordion-label">
					Accordion Label
					<svg>
						<use xlink:href="#arrowhead-down" />
					</svg>
				</button>
				<div class="accordion-contents">
					<p>Curabitur nisi nunc, bibendum ac efficitur vitae, efficitur sed felis. Praesent tempus erat tellus, sit amet interdum
						purus semper id. Phasellus interdum, dolor sit amet egestas auctor, turpis elit varius metus, eu congue felis diam
						lacinia ipsum. Nullam rutrum elit scelerisque risus venenatis laoreet. Curabitur maximus tellus sit amet odio maximus
						laoreet a eget sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum
						a arcu ipsum. Ut elementum commodo mollis.</p>
				</div>
				<button class="accordion-label">
					Accordion Label
					<svg>
						<use xlink:href="#arrowhead-down" />
					</svg>
				</button>
				<div class="accordion-contents">
					<p>Curabitur nisi nunc, bibendum ac efficitur vitae, efficitur sed felis. Praesent tempus erat tellus, sit amet interdum
						purus semper id. Phasellus interdum, dolor sit amet egestas auctor, turpis elit varius metus, eu congue felis diam
						lacinia ipsum. Nullam rutrum elit scelerisque risus venenatis laoreet. Curabitur maximus tellus sit amet odio maximus
						laoreet a eget sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum
						a arcu ipsum. Ut elementum commodo mollis.</p>
				</div>
				<button class="accordion-label">
					Accordion Label
					<svg>
						<use xlink:href="#arrowhead-down" />
					</svg>
				</button>
				<div class="accordion-contents">
					<p>Curabitur nisi nunc, bibendum ac efficitur vitae, efficitur sed felis. Praesent tempus erat tellus, sit amet interdum
						purus semper id. Phasellus interdum, dolor sit amet egestas auctor, turpis elit varius metus, eu congue felis diam
						lacinia ipsum. Nullam rutrum elit scelerisque risus venenatis laoreet. Curabitur maximus tellus sit amet odio maximus
						laoreet a eget sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum
						a arcu ipsum. Ut elementum commodo mollis.</p>
				</div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="accordion"&gt;
						&lt;button class="accordion-label"&gt;
							Accordion Label
							&lt;svg&gt;
								&lt;use xlink:href="#arrowhead-down" /&gt;
							&lt;/svg&gt;
						&lt;/button&gt;
						&lt;div class="accordion-contents"&gt;
							&lt;p&gt;Curabitur nisi nunc, bibendum ac efficitur vitae, efficitur sed felis. Praesent tempus erat tellus, sit amet interdum
								purus semper id. Phasellus interdum, dolor sit amet egestas auctor, turpis elit varius metus, eu congue felis diam
								lacinia ipsum. Nullam rutrum elit scelerisque risus venenatis laoreet. Curabitur maximus tellus sit amet odio maximus
								laoreet a eget sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum
								a arcu ipsum. Ut elementum commodo mollis.&lt;/p&gt;
						&lt;/div&gt;
						&lt;button class="accordion-label"&gt;
							Accordion Label
							&lt;svg&gt;
								&lt;use xlink:href="#arrowhead-down" /&gt;
							&lt;/svg&gt;
						&lt;/button&gt;
						&lt;div class="accordion-contents"&gt;
							&lt;p&gt;Curabitur nisi nunc, bibendum ac efficitur vitae, efficitur sed felis. Praesent tempus erat tellus, sit amet interdum
								purus semper id. Phasellus interdum, dolor sit amet egestas auctor, turpis elit varius metus, eu congue felis diam
								lacinia ipsum. Nullam rutrum elit scelerisque risus venenatis laoreet. Curabitur maximus tellus sit amet odio maximus
								laoreet a eget sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum
								a arcu ipsum. Ut elementum commodo mollis.&lt;/p&gt;
						&lt;/div&gt;
						&lt;button class="accordion-label"&gt;
							Accordion Label
							&lt;svg&gt;
								&lt;use xlink:href="#arrowhead-down" /&gt;
							&lt;/svg&gt;
						&lt;/button&gt;
						&lt;div class="accordion-contents"&gt;
							&lt;p&gt;Curabitur nisi nunc, bibendum ac efficitur vitae, efficitur sed felis. Praesent tempus erat tellus, sit amet interdum
								purus semper id. Phasellus interdum, dolor sit amet egestas auctor, turpis elit varius metus, eu congue felis diam
								lacinia ipsum. Nullam rutrum elit scelerisque risus venenatis laoreet. Curabitur maximus tellus sit amet odio maximus
								laoreet a eget sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum
								a arcu ipsum. Ut elementum commodo mollis.&lt;/p&gt;
						&lt;/div&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/accordions.postcss</li>
					<li>../master/accordions.js</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.has-one-active</h3>
			<p class="subheader">modifier to auto close other sections on selection</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="accordion has-one-active">
				<button class="accordion-label">
					Accordion Label
					<svg>
						<use xlink:href="#arrowhead-down" />
					</svg>
				</button>
				<div class="accordion-contents">
					<p>Curabitur nisi nunc, bibendum ac efficitur vitae, efficitur sed felis. Praesent tempus erat tellus, sit amet interdum
						purus semper id. Phasellus interdum, dolor sit amet egestas auctor, turpis elit varius metus, eu congue felis diam
						lacinia ipsum. Nullam rutrum elit scelerisque risus venenatis laoreet. Curabitur maximus tellus sit amet odio maximus
						laoreet a eget sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum
						a arcu ipsum. Ut elementum commodo mollis.</p>
				</div>
				<button class="accordion-label">
					Accordion Label
					<svg>
						<use xlink:href="#arrowhead-down" />
					</svg>
				</button>
				<div class="accordion-contents">
					<p>Curabitur nisi nunc, bibendum ac efficitur vitae, efficitur sed felis. Praesent tempus erat tellus, sit amet interdum
						purus semper id. Phasellus interdum, dolor sit amet egestas auctor, turpis elit varius metus, eu congue felis diam
						lacinia ipsum. Nullam rutrum elit scelerisque risus venenatis laoreet. Curabitur maximus tellus sit amet odio maximus
						laoreet a eget sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum
						a arcu ipsum. Ut elementum commodo mollis.</p>
				</div>
				<button class="accordion-label">
					Accordion Label
					<svg>
						<use xlink:href="#arrowhead-down" />
					</svg>
				</button>
				<div class="accordion-contents">
					<p>Curabitur nisi nunc, bibendum ac efficitur vitae, efficitur sed felis. Praesent tempus erat tellus, sit amet interdum
						purus semper id. Phasellus interdum, dolor sit amet egestas auctor, turpis elit varius metus, eu congue felis diam
						lacinia ipsum. Nullam rutrum elit scelerisque risus venenatis laoreet. Curabitur maximus tellus sit amet odio maximus
						laoreet a eget sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum
						a arcu ipsum. Ut elementum commodo mollis.</p>
				</div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="accordion has-one-active"&gt;
						&lt;button class="accordion-label"&gt;
							Accordion Label
							&lt;svg&gt;
								&lt;use xlink:href="#arrowhead-down" /&gt;
							&lt;/svg&gt;
						&lt;/button&gt;
						&lt;div class="accordion-contents"&gt;
							&lt;p&gt;Curabitur nisi nunc, bibendum ac efficitur vitae, efficitur sed felis. Praesent tempus erat tellus, sit amet interdum
								purus semper id. Phasellus interdum, dolor sit amet egestas auctor, turpis elit varius metus, eu congue felis diam
								lacinia ipsum. Nullam rutrum elit scelerisque risus venenatis laoreet. Curabitur maximus tellus sit amet odio maximus
								laoreet a eget sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum
								a arcu ipsum. Ut elementum commodo mollis.&lt;/p&gt;
						&lt;/div&gt;
						&lt;button class="accordion-label"&gt;
							Accordion Label
							&lt;svg&gt;
								&lt;use xlink:href="#arrowhead-down" /&gt;
							&lt;/svg&gt;
						&lt;/button&gt;
						&lt;div class="accordion-contents"&gt;
							&lt;p&gt;Curabitur nisi nunc, bibendum ac efficitur vitae, efficitur sed felis. Praesent tempus erat tellus, sit amet interdum
								purus semper id. Phasellus interdum, dolor sit amet egestas auctor, turpis elit varius metus, eu congue felis diam
								lacinia ipsum. Nullam rutrum elit scelerisque risus venenatis laoreet. Curabitur maximus tellus sit amet odio maximus
								laoreet a eget sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum
								a arcu ipsum. Ut elementum commodo mollis.&lt;/p&gt;
						&lt;/div&gt;
						&lt;button class="accordion-label"&gt;
							Accordion Label
							&lt;svg&gt;
								&lt;use xlink:href="#arrowhead-down" /&gt;
							&lt;/svg&gt;
						&lt;/button&gt;
						&lt;div class="accordion-contents"&gt;
							&lt;p&gt;Curabitur nisi nunc, bibendum ac efficitur vitae, efficitur sed felis. Praesent tempus erat tellus, sit amet interdum
								purus semper id. Phasellus interdum, dolor sit amet egestas auctor, turpis elit varius metus, eu congue felis diam
								lacinia ipsum. Nullam rutrum elit scelerisque risus venenatis laoreet. Curabitur maximus tellus sit amet odio maximus
								laoreet a eget sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum
								a arcu ipsum. Ut elementum commodo mollis.&lt;/p&gt;
						&lt;/div&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/accordions.postcss</li>
					<li>../master/accordions.js</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.has-icon</h3>
			<p class="subheader">modifier to add an icon to the label</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="accordion has-icon">
				<button class="accordion-label">
					Accordion Label
					<svg>
						<use xlink:href="#rocket" />
					</svg>
					<svg>
						<use xlink:href="#arrowhead-down" />
					</svg>
				</button>
				<div class="accordion-contents">
					<p>Curabitur nisi nunc, bibendum ac efficitur vitae, efficitur sed felis. Praesent tempus erat tellus, sit amet interdum
						purus semper id. Phasellus interdum, dolor sit amet egestas auctor, turpis elit varius metus, eu congue felis diam
						lacinia ipsum. Nullam rutrum elit scelerisque risus venenatis laoreet. Curabitur maximus tellus sit amet odio maximus
						laoreet a eget sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum
						a arcu ipsum. Ut elementum commodo mollis.</p>
				</div>
				<button class="accordion-label">
					Accordion Label
					<svg>
						<use xlink:href="#rocket" />
					</svg>
					<svg>
						<use xlink:href="#arrowhead-down" />
					</svg>
				</button>
				<div class="accordion-contents">
					<p>Curabitur nisi nunc, bibendum ac efficitur vitae, efficitur sed felis. Praesent tempus erat tellus, sit amet interdum
						purus semper id. Phasellus interdum, dolor sit amet egestas auctor, turpis elit varius metus, eu congue felis diam
						lacinia ipsum. Nullam rutrum elit scelerisque risus venenatis laoreet. Curabitur maximus tellus sit amet odio maximus
						laoreet a eget sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum
						a arcu ipsum. Ut elementum commodo mollis.</p>
				</div>
				<button class="accordion-label">
					Accordion Label
					<svg>
						<use xlink:href="#rocket" />
					</svg>
					<svg>
						<use xlink:href="#arrowhead-down" />
					</svg>
				</button>
				<div class="accordion-contents">
					<p>Curabitur nisi nunc, bibendum ac efficitur vitae, efficitur sed felis. Praesent tempus erat tellus, sit amet interdum
						purus semper id. Phasellus interdum, dolor sit amet egestas auctor, turpis elit varius metus, eu congue felis diam
						lacinia ipsum. Nullam rutrum elit scelerisque risus venenatis laoreet. Curabitur maximus tellus sit amet odio maximus
						laoreet a eget sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum
						a arcu ipsum. Ut elementum commodo mollis.</p>
				</div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="accordion has-icon"&gt;
						&lt;button class="accordion-label"&gt;
							Accordion Label
							&lt;svg&gt;
								&lt;use xlink:href="#rocket" /&gt;
							&lt;/svg&gt;
							&lt;svg&gt;
								&lt;use xlink:href="#arrowhead-down" /&gt;
							&lt;/svg&gt;
						&lt;/button&gt;
						&lt;div class="accordion-contents"&gt;
							&lt;p&gt;Curabitur nisi nunc, bibendum ac efficitur vitae, efficitur sed felis. Praesent tempus erat tellus, sit amet interdum
								purus semper id. Phasellus interdum, dolor sit amet egestas auctor, turpis elit varius metus, eu congue felis diam
								lacinia ipsum. Nullam rutrum elit scelerisque risus venenatis laoreet. Curabitur maximus tellus sit amet odio maximus
								laoreet a eget sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum
								a arcu ipsum. Ut elementum commodo mollis.&lt;/p&gt;
						&lt;/div&gt;
						&lt;button class="accordion-label"&gt;
							Accordion Label
							&lt;svg&gt;
								&lt;use xlink:href="#rocket" /&gt;
							&lt;/svg&gt;
							&lt;svg&gt;
								&lt;use xlink:href="#arrowhead-down" /&gt;
							&lt;/svg&gt;
						&lt;/button&gt;
						&lt;div class="accordion-contents"&gt;
							&lt;p&gt;Curabitur nisi nunc, bibendum ac efficitur vitae, efficitur sed felis. Praesent tempus erat tellus, sit amet interdum
								purus semper id. Phasellus interdum, dolor sit amet egestas auctor, turpis elit varius metus, eu congue felis diam
								lacinia ipsum. Nullam rutrum elit scelerisque risus venenatis laoreet. Curabitur maximus tellus sit amet odio maximus
								laoreet a eget sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum
								a arcu ipsum. Ut elementum commodo mollis.&lt;/p&gt;
						&lt;/div&gt;
						&lt;button class="accordion-label"&gt;
							Accordion Label
							&lt;svg&gt;
								&lt;use xlink:href="#rocket" /&gt;
							&lt;/svg&gt;
							&lt;svg&gt;
								&lt;use xlink:href="#arrowhead-down" /&gt;
							&lt;/svg&gt;
						&lt;/button&gt;
						&lt;div class="accordion-contents"&gt;
							&lt;p&gt;Curabitur nisi nunc, bibendum ac efficitur vitae, efficitur sed felis. Praesent tempus erat tellus, sit amet interdum
								purus semper id. Phasellus interdum, dolor sit amet egestas auctor, turpis elit varius metus, eu congue felis diam
								lacinia ipsum. Nullam rutrum elit scelerisque risus venenatis laoreet. Curabitur maximus tellus sit amet odio maximus
								laoreet a eget sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum
								a arcu ipsum. Ut elementum commodo mollis.&lt;/p&gt;
						&lt;/div&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/accordions.postcss</li>
					<li>../master/accordions.js</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
</section>