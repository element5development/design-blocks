<section class="design-block">
	<h2>Navigations</h2>
	<div class="modifer">
		<div class="modifer-header">
			<h3>Default Navigation</h3>
			<p class="subheader">a standard navigation</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<nav>
				<ul>
					<li>
						<a href="#">Item One</a>
					</li>
					<li>
						<a href="#">Item Two</a>
					</li>
					<li>
						<a href="#">Item Three</a>
					</li>
					<li>
						<a href="#">Item Four</a>
					</li>
					<li>
						<a href="#">Item Five</a>
					</li>
				</ul>
			</nav>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;nav&gt;
						&lt;ul&gt;
							&lt;li&gt;
								&lt;a href="#"&gt;Item One&lt;/a&gt;
							&lt;/li&gt;
							&lt;li&gt;
								&lt;a href="#"&gt;Item Two&lt;/a&gt;
							&lt;/li&gt;
							&lt;li&gt;
								&lt;a href="#"&gt;Item Three&lt;/a&gt;
							&lt;/li&gt;
							&lt;li&gt;
								&lt;a href="#"&gt;Item Four&lt;/a&gt;
							&lt;/li&gt;
							&lt;li&gt;
								&lt;a href="#"&gt;Item Five&lt;/a&gt;
							&lt;/li&gt;
						&lt;/ul&gt;
					&lt;/nav&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/navigation.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.has-dropdown</h3>
			<p class="subheader">a standard navigation</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<nav class="has-dropdown">
				<ul>
					<li class="menu-item-has-children">
						<a href="#">Item One</a>
						<ul class="sub-menu">
							<li>
								<a href="#">Sub Item One</a>
							</li>
							<li>
								<a href="#">Sub Item Two</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="#">Item Two</a>
					</li>
					<li>
						<a href="#">Item Three</a>
					</li>
					<li>
						<a href="#">Item Four</a>
					</li>
					<li>
						<a href="#">Item Five</a>
					</li>
				</ul>
			</nav>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;nav class="has-dropdown"&gt;
						&lt;ul&gt;
							&lt;li class="menu-item-has-children"&gt;
								&lt;a href="#"&gt;Item One&lt;/a&gt;
								&lt;ul class="sub-menu"&gt;
									&lt;li&gt;
										&lt;a href="#"&gt;Sub Item One&lt;/a&gt;
									&lt;/li&gt;
									&lt;li&gt;
										&lt;a href="#"&gt;Sub Item Two&lt;/a&gt;
									&lt;/li&gt;
								&lt;/ul&gt;
							&lt;/li&gt;
							&lt;li&gt;
								&lt;a href="#"&gt;Item Two&lt;/a&gt;
							&lt;/li&gt;
							&lt;li&gt;
								&lt;a href="#"&gt;Item Three&lt;/a&gt;
							&lt;/li&gt;
							&lt;li&gt;
								&lt;a href="#"&gt;Item Four&lt;/a&gt;
							&lt;/li&gt;
							&lt;li&gt;
								&lt;a href="#"&gt;Item Five&lt;/a&gt;
							&lt;/li&gt;
						&lt;/ul&gt;
					&lt;/nav&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/navigation.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.is-stacked</h3>
			<p class="subheader">a standard navigation</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<nav class="is-stacked">
				<ul>
					<li>
						<a href="#">Item One</a>
					</li>
					<li>
						<a href="#">Item Two</a>
					</li>
					<li>
						<a href="#">Item Three</a>
					</li>
					<li>
						<a href="#">Item Four</a>
					</li>
					<li>
						<a href="#">Item Five</a>
					</li>
				</ul>
			</nav>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;nav&gt;
						&lt;ul&gt;
							&lt;li&gt;
								&lt;a href="#"&gt;Item One&lt;/a&gt;
							&lt;/li&gt;
							&lt;li&gt;
								&lt;a href="#"&gt;Item Two&lt;/a&gt;
							&lt;/li&gt;
							&lt;li&gt;
								&lt;a href="#"&gt;Item Three&lt;/a&gt;
							&lt;/li&gt;
							&lt;li&gt;
								&lt;a href="#"&gt;Item Four&lt;/a&gt;
							&lt;/li&gt;
							&lt;li&gt;
								&lt;a href="#"&gt;Item Five&lt;/a&gt;
							&lt;/li&gt;
						&lt;/ul&gt;
					&lt;/nav&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/navigation.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
</section>