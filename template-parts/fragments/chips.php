<section class="design-block">
	<h2>Chips</h2>
	<div class="modifer">
		<div class="modifer-header">
			<h3>Default Chip</h3>
			<p class="subheader">a standard chip</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="chip" href="#">default</div>
			<div class="chip is-hover" href="#">hover</div>
			<div class="chip is-active" href="#">active</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;a class="chip" href="#"&gt;default&lt;/a&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/chips.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.has-icon</h3>
			<p class="subheader">modifier to include an icon</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="chip has-icon">
				default
				<svg>
					<use xlink:href="#close" />
				</svg>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="chip has-icon"&gt;
						default
						&lt;svg&gt;
							&lt;use xlink:href="#close" /&gt;
						&lt;/svg&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/chips.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.is-[color]</h3>
			<p class="subheader">modifier to adjust the chips' color</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="chip is-primary">primary</div>
			<div class="chip is-secondary">secondary</div>
			<div class="chip is-tertiary">tertiary</div>
			<div class="chip is-negative">negative</div>
			<div class="chip is-warning">warning</div>
			<div class="chip is-positive">positive</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="chip is-primary"&gt;primary&lt;/div&gt;
					&lt;div class="chip is-secondary"&gt;secondary&lt;/div&gt;
					&lt;div class="chip is-tertiary"&gt;tertiary&lt;/div&gt;
					&lt;div class="chip is-negative"&gt;negative&lt;/div&gt;
					&lt;div class="chip is-warning"&gt;warning&lt;/div&gt;
					&lt;div class="chip is-positive"&gt;positive&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/chips.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.is-ghost</h3>
			<p class="subheader">modifier to remove the chip's background color</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="chip is-ghost">default</div>
			<div class="chip is-primary is-ghost">primary</div>
			<div class="chip is-secondary is-ghost">secondary</div>
			<div class="chip is-tertiary is-ghost">tertiary</div>
			<div class="chip is-negative is-ghost">negative</div>
			<div class="chip is-warning is-ghost">warning</div>
			<div class="chip is-positive is-ghost">positive</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;a class="chip is-ghost"&gt;label&lt;/a&gt;
					&lt;a class="chip is-primary is-ghost"&gt;primary&lt;/a&gt;
					&lt;a class="chip is-secondary is-ghost"&gt;secondary&lt;/a&gt;
					&lt;a class="chip is-tertiary is-ghost"&gt;tertiary&lt;/a&gt;
					&lt;a class="chip is-negative is-ghost"&gt;negative&lt;/a&gt;
					&lt;a class="chip is-warning is-ghost"&gt;warning&lt;/a&gt;
					&lt;a class="chip is-positive is-ghost"&gt;positive&lt;/a&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/chips.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.is-icon</h3>
			<p class="subheader">modifier to be used when the chip consists only of a icon</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="chip is-icon">
				<svg>
					<use xlink:href="#atom" />
				</svg>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="chip is-icon"&gt;
						&lt;svg&gt;
							&lt;use xlink:href="#atom" /&gt;
						&lt;/svg&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/buttons.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.on-corner</h3>
			<p class="subheader">modifier to be used when chip is suppose to be placed in the corner of parent element</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="block">
				<div class="chip is-icon on-corner">
					<svg>
						<use xlink:href="#atom" />
					</svg>
				</div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="chip is-icon on-corner"&gt;
						&lt;svg&gt;
							&lt;use xlink:href="#atom" /&gt;
						&lt;/svg&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/buttons.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
</section>
<section class="design-block">
	<h2>Gouped Chips</h2>
	<div class="modifer">
		<div class="modifer-header">
			<h3>Default Chips</h3>
			<p class="subheader">standard grouped chips</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="chips">
				<div class="chip">One</div>
				<div class="chip">Two</div>
				<div class="chip">Three</div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="chips"&gt;
						&lt;div class="chip"&gt;One&lt;/div&gt;
						&lt;div class="chip"&gt;Two&lt;/div&gt;
						&lt;div class="chip"&gt;Three&ltdiv/a&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/chips.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.are-stacked</h3>
			<p class="subheader">modifier to make chips stack on top of each other</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="chips are-stacked">
				<div class="chip">One</div>
				<div class="chip">Two</div>
				<div class="chip">Three</div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="chips are-stacked"&gt;
						&lt;div class="chip"&gt;One&lt;/div&gt;
						&lt;div class="chip"&gt;Two&lt;/div&gt;
						&lt;div class="chip"&gt;Three&lt;/div&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/chips.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
</section>