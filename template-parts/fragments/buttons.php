<section class="design-block">
	<h2>Buttons</h2>
	<div class="modifer">
		<div class="modifer-header">
			<h3>Default Button</h3>
			<p class="subheader">a standard button</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<a class="button" href="#">default</a>
			<a class="button is-hover" href="#">hover</a>
			<a class="button is-active" href="#">active</a>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;a class="button" href="#"&gt;Text&lt;/a&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/buttons.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.has-icon</h3>
			<p class="subheader">modifier to include an icon</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<a class="button has-icon" href="#">
				default
				<svg>
					<use xlink:href="#arrowhead-right" />
				</svg>
			</a>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;a class="button has-icon" href="#"&gt;
						label
						&lt;svg&gt;
							&lt;use xlink:href="#code" /&gt;
						&lt;/svg&gt;
					&lt;/a&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/buttons.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.is-[size]</h3>
			<p class="subheader">modifier to adjust the button's size</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<a class="button is-small" href="#">small</a>
			<a class="button is-massive" href="#">massive</a>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
						&lt;a class="button is-small" href="#"&gt;small&lt;/a&gt;
						&lt;a class="button is-massive" href="#"&gt;massive&lt;/a&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/buttons.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.is-fluid</h3>
			<p class="subheader">modifier to make button fill it's parent width</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<a class="button is-fluid" href="#">label</a>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;a class="button is-fluid" href="#"&gt;label&lt;/a&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/buttons.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.is-[color]</h3>
			<p class="subheader">modifier to adjust the button's color</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<a class="button is-primary" href="#">primary</a>
			<a class="button is-secondary" href="#">secondary</a>
			<a class="button is-tertiary" href="#">tertiary</a>
			<a class="button is-negative" href="#">negative</a>
			<a class="button is-warning" href="#">warning</a>
			<a class="button is-positive" href="#">positive</a>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;a class="button is-primary" href="#"&gt;primary&lt;/a&gt;
					&lt;a class="button is-secondary" href="#"&gt;secondary&lt;/a&gt;
					&lt;a class="button is-tertiary" href="#"&gt;tertiary&lt;/a&gt;
					&lt;a class="button is-negative" href="#"&gt;negative&lt;/a&gt;
					&lt;a class="button is-warning" href="#"&gt;warning&lt;/a&gt;
					&lt;a class="button is-positive" href="#"&gt;positive&lt;/a&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/buttons.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.is-ghost</h3>
			<p class="subheader">modifier to remove the button's background color</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<a class="button is-ghost" href="#">default</a>
			<a class="button is-primary is-ghost" href="#">primary</a>
			<a class="button is-secondary is-ghost" href="#">secondary</a>
			<a class="button is-tertiary is-ghost" href="#">tertiary</a>
			<a class="button is-negative is-ghost" href="#">negative</a>
			<a class="button is-warning is-ghost" href="#">warning</a>
			<a class="button is-positive is-ghost" href="#">positive</a>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;a class="button is-ghost" href="#"&gt;label&lt;/a&gt;
					&lt;a class="button is-primary is-ghost" href="#"&gt;primary&lt;/a&gt;
					&lt;a class="button is-secondary is-ghost" href="#"&gt;secondary&lt;/a&gt;
					&lt;a class="button is-tertiary is-ghost" href="#"&gt;tertiary&lt;/a&gt;
					&lt;a class="button is-negative is-ghost" href="#"&gt;negative&lt;/a&gt;
					&lt;a class="button is-warning is-ghost" href="#"&gt;warning&lt;/a&gt;
					&lt;a class="button is-positive is-ghost" href="#"&gt;positive&lt;/a&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/buttons.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.is-borderless</h3>
			<p class="subheader">modifier to remove both the buttons background color and border color</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<a class="button is-borderless" href="#">default</a>
			<a class="button is-primary is-borderless" href="#">primary</a>
			<a class="button is-secondary is-borderless" href="#">secondary</a>
			<a class="button is-tertiary is-borderless" href="#">tertiary</a>
			<a class="button is-negative is-borderless" href="#">negative</a>
			<a class="button is-warning is-borderless" href="#">warning</a>
			<a class="button is-positive is-borderless" href="#">positive</a>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;a class="button is-borderless" href="#"&gt;default&lt;/a&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/buttons.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.is-icon</h3>
			<p class="subheader">modifier to be used when the button consists only of a icon</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<a class="button is-icon" href="#">
				<svg>
					<use xlink:href="#atom" />
				</svg>
			</a>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;a class="button is-icon" href="#"&gt;
						&lt;svg&gt;
							&lt;use xlink:href="#atom" /&gt;
						&lt;/svg&gt;
					&lt;/a&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/buttons.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
</section>
<section class="design-block">
	<h2>Gouped Buttons</h2>
	<div class="modifer">
		<div class="modifer-header">
			<h3>Default Buttons</h3>
			<p class="subheader">standard grouped buttons</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="buttons">
				<a class="button" href="#">One</a>
				<a class="button" href="#">Two</a>
				<a class="button" href="#">Three</a>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="buttons"&gt;
						&lt;a class="button" href="#"&gt;One&lt;/a&gt;
						&lt;a class="button" href="#"&gt;Two&lt;/a&gt;
						&lt;a class="button" href="#"&gt;Three&lt;/a&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/buttons.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.are-grouped</h3>
			<p class="subheader">modifier to remove spacing between grouped buttons</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="buttons are-grouped">
				<a class="button" href="#">One</a>
				<a class="button" href="#">Two</a>
				<a class="button" href="#">Three</a>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="buttons are-grouped"&gt;
						&lt;a class="button" href="#"&gt;One&lt;/a&gt;
						&lt;a class="button" href="#"&gt;Two&lt;/a&gt;
						&lt;a class="button" href="#"&gt;Three&lt;/a&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/buttons.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
</section>