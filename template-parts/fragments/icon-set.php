<svg xmlns="http://www.w3.org/2000/svg" style="display: none">
	<symbol id="code" viewBox="0 0 64 48">
		<path d="M20.5 10.5l-2.8-2.8c-.4-.4-1-.4-1.4 0L.7 23.3c-.2.2-.3.5-.3.7s.1.5.3.7l15.6 15.6c.4.4 1 .4 1.4 0l2.8-2.8c.4-.4.4-1 0-1.4l-12-12 12-12c.4-.5.4-1.2 0-1.6zm42.7 12.8L47.7 7.7c-.4-.4-1-.4-1.4 0l-2.8 2.8c-.4.4-.4 1 0 1.4l12 12-12 12c-.4.4-.4 1 0 1.4l2.8 2.8c.4.4 1 .4 1.4 0l15.6-15.6c.2-.2.3-.5.3-.7-.1-.1-.2-.3-.4-.5zm-23.7-22c-1.6-.5-3.3.4-3.8 1.9l-13 39.9c-.5 1.6.4 3.3 1.9 3.8s3.3-.4 3.8-1.9l13-40c.5-1.5-.3-3.2-1.9-3.7z"></path>
	</symbol>
	<symbol id="arrowhead-right" viewBox="0 0 64 64">
		<path data-name="layer1" stroke-miterlimit="10" stroke-width="4" d="M26 20.006L40 32 26 44.006" stroke-linejoin="round" stroke-linecap="round"></path>
	</symbol>
	<symbol id="arrowhead-left" viewBox="0 0 64 64">
		<path data-name="layer1" stroke-miterlimit="10" stroke-width="4" d="M40 44.006L26 32.012l14-12.006" stroke-linejoin="round"
		  stroke-linecap="round"></path>
	</symbol>
	<symbol id="arrowhead-down" viewBox="0 0 64 64">
		<path data-name="layer1" stroke-miterlimit="10" stroke-width="2" d="M20 26l11.994 14L44 26" stroke-linejoin="round" stroke-linecap="round"></path>
	</symbol>
	<symbol id="atom" viewBox="0 0 64 64">
		<path data-name="layer1" d="M36.929 23.453C22.843 15.373 9.173 12.7 6.4 17.475s6.391 15.2 20.476 23.283S54.628 51.514 57.4 46.735s-6.386-15.202-20.471-23.282z"
		  stroke-linecap="round" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="round"></path>
		<circle data-name="layer2" cx="32" cy="32" r="4" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="round"></circle>
		<path data-name="layer2" d="M23.333 46.973a77.044 77.044 0 0 0 13.6-6.215c14.086-8.08 23.253-18.5 20.476-23.283-1.832-3.153-8.406-3.061-16.712-.313m-3.713 1.36a81.213 81.213 0 0 0-10.111 4.93C12.787 31.533 3.62 41.957 6.4 46.735c1.6 2.748 6.8 3.031 13.6 1.238"
		  stroke-linecap="round" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="round"></path>
		<path data-name="layer1" d="M22.177 26.349C23.058 12.482 27.121 2 32 2c5.523 0 10 13.431 10 30a89.7 89.7 0 0 1-.182 5.733m-.393 4.319C40.047 53.677 36.348 62 32 62c-5.523 0-10-13.431-10-30"
		  stroke-linecap="round" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="round"></path>
	</symbol>
	<symbol id="rocket" viewBox="0 0 64 64">
		<path data-name="layer2" d="M11.1 47.3C5.4 50 1.7 54.9 2 62c7.1.4 12-3.4 14.7-9.1" stroke-linecap="round" stroke-linejoin="round"
		  stroke-width="2"></path>
		<path data-name="layer1" d="M62 2s-13.4-.7-22.6 8.5S8.3 44.4 8.3 44.4l11.3 11.4s24.7-22 33.9-31.2S62 2 62 2z" stroke-linecap="round"
		  stroke-linejoin="round" stroke-width="2"></path>
		<circle data-name="layer2" cx="44" cy="20" r="4" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></circle>
		<path data-name="layer1" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13.6 38.5l12 11.9M24.8 26H12.5l-7.7 7.7 11 2.3M37 40.2v12.3l-7.7 7.6L27 49.2"></path>
	</symbol>
	<symbol id="close" viewBox="0 0 64 64">
		<circle data-name="layer2" cx="32.001" cy="32" r="30" transform="rotate(-45 32.001 32)" stroke-miterlimit="10" stroke-width="2"
		  stroke-linejoin="round" stroke-linecap="round"></circle>
		<path data-name="layer1" stroke-miterlimit="10" stroke-width="2" d="M42.999 21.001l-22 22m22 0L21 21" stroke-linejoin="round"
		  stroke-linecap="round"></path>
	</symbol>
	<symbol id="person" viewBox="0 0 64 64">
		<path data-name="layer1" d="M46 26c0 6.1-3.4 11.5-7 14.8V44c0 2 1 5.1 11 7a15.5 15.5 0 0 1 12 11H2a13.4 13.4 0 0 1 11-11c10-1.8 12-5 12-7v-3.2c-3.6-3.3-7-8.6-7-14.8v-9.6C18 6 25.4 2 32 2s14 4 14 14.4z"
		  stroke-miterlimit="10" stroke-width="2" stroke-linejoin="round" stroke-linecap="round"></path>
	</symbol>
</svg>