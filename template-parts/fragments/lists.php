<section class="design-block">
	<h2>Lists</h2>
	<div class="modifer">
		<div class="modifer-header">
			<h3>Default Lists</h3>
			<p class="subheader">a standard ordered and unordered list</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="grid has-two-column">
				<div class="grid-blog">
					<ol>
						<li>First Item</li>
						<li>Second Item</li>
						<li>Third Item</li>
					</ol>
				</div>
				<div class="grid-blog">
					<ul>
						<li>First Item</li>
						<li>Second Item</li>
						<li>Third Item</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;ol&gt;
						&lt;li&gt;First Item&lt;/li&gt;
						&lt;li&gt;Second Item&lt;/li&gt;
						&lt;li&gt;Third Item&lt;/li&gt;
					&lt;/ol&gt;
					&lt;ul&gt;
						&lt;li&gt;First Item&lt;/li&gt;
						&lt;li&gt;Second Item&lt;/li&gt;
						&lt;li&gt;Third Item&lt;/li&gt;
					&lt;/ul&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/lists.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.have-no-bullets</h3>
			<p class="subheader">modifer to remove bullets for each list item</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<ul class="have-no-bullets">
				<li>First Item</li>
				<li>Second Item</li>
				<li>Third Item</li>
			</ul>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;ul class="have-no-bullets"&gt;
						&lt;li&gt;First Item&lt;/li&gt;
						&lt;li&gt;Second Item&lt;/li&gt;
						&lt;li&gt;Third Item&lt;/li&gt;
					&lt;/ul&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/lists.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.have-icons</h3>
			<p class="subheader">modifier to include an icon with every list item</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<ul class="have-icons">
				<li>
					<svg>
						<use xlink:href="#person" />
					</svg>
					First Item
				</li>
				<li>
					<svg>
						<use xlink:href="#person" />
					</svg>
					Second Item
				</li>
				<li>
					<svg>
						<use xlink:href="#person" />
					</svg>
					Third Item
				</li>
			</ul>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;ul class="have-icons"&gt;
						&lt;li&gt;
							&lt;svg&gt;
								&lt;use xlink:href="#person" /&gt;
							&lt;/svg&gt;
						First Item
						&lt;/li&gt;
						&lt;li&gt;
							&lt;svg&gt;
								&lt;use xlink:href="#person" /&gt;
							&lt;/svg&gt;
						Second Item
						&lt;/li&gt;
						&lt;li&gt;
							&lt;svg&gt;
								&lt;use xlink:href="#person" /&gt;
							&lt;/svg&gt;
						Third Item
						&lt;/li&gt;
					&lt;/ul&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/lists.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.have-description</h3>
			<p class="subheader">modifier to add a description with every list item</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<ul class="have-description">
				<li>
					First Item
					<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi condimentum metus nec viverra sodales.</span>
				</li>
				<li>
					Second Item
					<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi condimentum metus nec viverra sodales.</span>
				</li>
				<li>
					Third Item
					<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi condimentum metus nec viverra sodales.</span>
				</li>
			</ul>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;ul class="have-description"&gt;
						&lt;li&gt;
						First Item
							&lt;span&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi condimentum metus nec viverra sodales.	&lt;/span&gt;
						&lt;/li&gt;
						&lt;li&gt;
						Second Item
							&lt;span&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi condimentum metus nec viverra sodales.	&lt;/span&gt;
						&lt;/li&gt;
						&lt;li&gt;
						Third Item
							&lt;span&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi condimentum metus nec viverra sodales.	&lt;/span&gt;
						&lt;/li&gt;
					&lt;/ul&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/lists.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.are-inline</h3>
			<p class="subheader">modifer to display list items side by side</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<ul class="are-inline">
				<li>First Item</li>
				<li>Second Item</li>
				<li>Third Item</li>
			</ul>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;ul class="are-inline"&gt;
						&lt;li&gt;First Item&lt;/li&gt;
						&lt;li&gt;Second Item&lt;/li&gt;
						&lt;li&gt;Third Item&lt;/li&gt;
					&lt;/ul&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/lists.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
</section>