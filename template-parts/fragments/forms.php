<section class="design-block">
	<h2>Forms & Labels</h2>
	<div class="modifer">
		<div class="modifer-header">
			<h3>Default Forms</h3>
			<p class="subheader">a standard form</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/forms.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
</section>