<section class="design-block design-grids">
	<h2>Standard Grids</h2>
	<div class="modifer">
		<div class="modifer-header">
			<h3>Default</h3>
			<!-- <p class="subheader"></p> -->
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="grid">
				<div class="grid-block"></div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="grid"&gt;
						&lt;div class="grid-block"&gt;&lt;/div&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/grid.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.has-two-column</h3>
			<!-- <p class="subheader"></p> -->
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="grid has-two-column">
				<div class="grid-block"></div>
				<div class="grid-block"></div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="grid has-two-column"&gt;
						&lt;div class="grid-block"&gt;&lt;/div&gt;
						&lt;div class="grid-block"&gt;&lt;/div&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/buttons.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.has-three-column</h3>
			<!-- <p class="subheader"></p> -->
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="grid has-three-column">
				<div class="grid-block"></div>
				<div class="grid-block"></div>
				<div class="grid-block"></div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="grid has-three-column"&gt;
						&lt;div class="grid-block"&gt;&lt;/div&gt;
						&lt;div class="grid-block"&gt;&lt;/div&gt;
						&lt;div class="grid-block"&gt;&lt;/div&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/buttons.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.has-four-column</h3>
			<!-- <p class="subheader"></p> -->
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="grid has-four-column">
				<div class="grid-block"></div>
				<div class="grid-block"></div>
				<div class="grid-block"></div>
				<div class="grid-block"></div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="grid has-four-column"&gt;
						&lt;div class="grid-block"&gt;&lt;/div&gt;
						&lt;div class="grid-block"&gt;&lt;/div&gt;
						&lt;div class="grid-block"&gt;&lt;/div&gt;
						&lt;div class="grid-block"&gt;&lt;/div&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/buttons.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.has-six-column</h3>
			<!-- <p class="subheader"></p> -->
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="grid has-six-column">
				<div class="grid-block"></div>
				<div class="grid-block"></div>
				<div class="grid-block"></div>
				<div class="grid-block"></div>
				<div class="grid-block"></div>
				<div class="grid-block"></div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="grid has-six-column"&gt;
						&lt;div class="grid-block"&gt;&lt;/div&gt;
						&lt;div class="grid-block"&gt;&lt;/div&gt;
						&lt;div class="grid-block"&gt;&lt;/div&gt;
						&lt;div class="grid-block"&gt;&lt;/div&gt;
						&lt;div class="grid-block"&gt;&lt;/div&gt;
						&lt;div class="grid-block"&gt;&lt;/div&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/buttons.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
</section>