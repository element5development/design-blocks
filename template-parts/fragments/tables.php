<section class="design-block">
	<h2>Tables</h2>
	<div class="modifer">
		<div class="modifer-header">
			<h3>Default Table</h3>
			<p class="subheader">modifer to display the tooltip at the top of parent</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<table>
				<tr>
					<th>Heading One</th>
					<th>Heading Two</th>
					<th>Heading Three</th>
				</tr>
				<tr>
					<td>Item One A</td>
					<td>Item One B</td>
					<td>Item One C</td>
				</tr>
				<tr>
					<td>Item Two A</td>
					<td>Item Two B</td>
					<td>Item Two C</td>
				</tr>
			</table>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;table&gt;
						&lt;tr&gt;
							&lt;th&gt;Heading One&lt;/th&gt;
							&lt;th&gt;Heading Two&lt;/th&gt;
							&lt;th&gt;Heading Three&lt;/th&gt;
						&lt;/tr&gt;
						&lt;tr&gt;
							&lt;td&gt;Item One A&lt;/td&gt;
							&lt;td&gt;Item One B&lt;/td&gt;
							&lt;td&gt;Item One C&lt;/td&gt;
						&lt;/tr&gt;
						&lt;tr&gt;
							&lt;td&gt;Item Two A&lt;/td&gt;
							&lt;td&gt;Item Two B&lt;/td&gt;
							&lt;td&gt;Item Two C&lt;/td&gt;
						&lt;/tr&gt;
					&lt;/table&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/tables.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
</section>