<section class="design-block">
	<h2>Typography</h2>
	<div class="grid has-two-column">
		<div class="grid-block">
			<h1>Heading One</h1>
			<h2>Heading Two</h2>
			<h3>Heading Three</h3>
			<h4>Heading Four</h4>
			<hr>
		</div>
		<div class="grid-block">
			<p>
				paragraph Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit
				<b>bold words</b> facilisis leo, ut dictum purus euismod a. purus mi, imperdiet a eros et,
				<a href="#">Link text</a>. Nullam vulputate, velit et cursus aliquet, ante nisl maximus velit, vitae aliquam enim nunc vitae ante.
			</p>
			<blockquote>
				<p>block quote, sit amet laoreet nisi. Suspendisse potenti. Nulla luctus ac orci consectetur pellentesque. Sed dignissim
					posuere felis, quis efficitur quam convallis et.</p>
				<p>Author Name</p>
			</blockquote>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.has-subheader</h3>
			<p class="subheader">modifer to add a subheadline</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<h2 class="has-subheader">Heading with subheader</h2>
			<p class="subheader">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus bibendum urna ac metus porttitor.</p>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;h2 class="has-subheader"&gt;Heading with subheader&lt;/h2&gt;
					&lt;p class="subheader"&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus bibendum urna ac metus porttitor.&lt;/p&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/typography.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.has-icon</h3>
			<p class="subheader">modifer to add an icon</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<h4 class="has-icon">
				<svg>
					<use xlink:href="#rocket" />
				</svg>
				Aliquam congue nibh
			</h4>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;h4 class="has-icon"&gt;
						&lt;svg&gt;
							&lt;use xlink:href="#rocket" /&gt;
						&lt;/svg&gt;
						Aliquam congue nibh
					&lt;/h4&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/typography.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.is-two-column</h3>
			<p class="subheader">modifer to split a single paragraph into 2 columns</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<p class="is-two-column">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus bibendum urna ac metus porttitor, in eleifend est elementum.
				Fusce ut est a nisi feugiat imperdiet. Morbi quis nibh pretium risus semper mollis. Phasellus luctus pharetra congue.
				Aliquam sed pulvinar ligula, et suscipit odio. Integer tristique nulla lectus, a tempus mi pharetra non. Donec eget urna
				eget erat tempus commodo. Vivamus efficitur est vel neque rutrum vestibulum. In hac habitasse platea dictumst. Integer
				vel mollis lorem. Donec cursus non neque eget molestie. Aliquam aliquam in massa ac feugiat. Suspendisse et nisi vel
				enim aliquet facilisis ut a dolor. Maecenas ornare pellentesque porta. Quisque et tempor est. Nullam sapien diam, efficitur
				in leo vitae, hendrerit laoreet lacus.</p>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;p class="is-two-column"&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus bibendum urna ac metus porttitor, in eleifend est elementum. Fusce ut est a nisi feugiat imperdiet. Morbi quis nibh pretium risus semper mollis. Phasellus luctus pharetra congue. Aliquam sed pulvinar ligula, et suscipit odio. Integer tristique nulla lectus, a tempus mi pharetra non. Donec eget urna eget erat tempus commodo. Vivamus efficitur est vel neque rutrum vestibulum. In hac habitasse platea dictumst. Integer vel mollis lorem. Donec cursus non neque eget molestie. Aliquam aliquam in massa ac feugiat. Suspendisse et nisi vel enim aliquet facilisis ut a dolor. Maecenas ornare pellentesque porta. Quisque et tempor est. Nullam sapien diam, efficitur in leo vitae, hendrerit laoreet lacus.&lt;/p&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/typography.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.is-three-column</h3>
			<p class="subheader">modifer to split a single paragraph into 3 columns</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<p class="is-three-column">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus bibendum urna ac metus porttitor, in eleifend est elementum.
				Fusce ut est a nisi feugiat imperdiet. Morbi quis nibh pretium risus semper mollis. Phasellus luctus pharetra congue.
				Aliquam sed pulvinar ligula, et suscipit odio. Integer tristique nulla lectus, a tempus mi pharetra non. Donec eget urna
				eget erat tempus commodo. Vivamus efficitur est vel neque rutrum vestibulum. In hac habitasse platea dictumst. Integer
				vel mollis lorem. Donec cursus non neque eget molestie. Aliquam aliquam in massa ac feugiat. Suspendisse et nisi vel
				enim aliquet facilisis ut a dolor. Maecenas ornare pellentesque porta. Quisque et tempor est. Nullam sapien diam, efficitur
				in leo vitae, hendrerit laoreet lacus.</p>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;p class="is-three-column"&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus bibendum urna ac metus porttitor, in eleifend est elementum. Fusce ut est a nisi feugiat imperdiet. Morbi quis nibh pretium risus semper mollis. Phasellus luctus pharetra congue. Aliquam sed pulvinar ligula, et suscipit odio. Integer tristique nulla lectus, a tempus mi pharetra non. Donec eget urna eget erat tempus commodo. Vivamus efficitur est vel neque rutrum vestibulum. In hac habitasse platea dictumst. Integer vel mollis lorem. Donec cursus non neque eget molestie. Aliquam aliquam in massa ac feugiat. Suspendisse et nisi vel enim aliquet facilisis ut a dolor. Maecenas ornare pellentesque porta. Quisque et tempor est. Nullam sapien diam, efficitur in leo vitae, hendrerit laoreet lacus.&lt;/p&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/typography.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.is-four-column</h3>
			<p class="subheader">modifer to split a single paragraph into 4 columns</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<p class="is-four-column">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus bibendum urna ac metus porttitor, in eleifend est elementum.
				Fusce ut est a nisi feugiat imperdiet. Morbi quis nibh pretium risus semper mollis. Phasellus luctus pharetra congue.
				Aliquam sed pulvinar ligula, et suscipit odio. Integer tristique nulla lectus, a tempus mi pharetra non. Donec eget urna
				eget erat tempus commodo. Vivamus efficitur est vel neque rutrum vestibulum. In hac habitasse platea dictumst. Integer
				vel mollis lorem. Donec cursus non neque eget molestie. Aliquam aliquam in massa ac feugiat. Suspendisse et nisi vel
				enim aliquet facilisis ut a dolor. Maecenas ornare pellentesque porta. Quisque et tempor est. Nullam sapien diam, efficitur
				in leo vitae, hendrerit laoreet lacus.</p>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;p class="is-four-column"&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus bibendum urna ac metus porttitor, in eleifend est elementum. Fusce ut est a nisi feugiat imperdiet. Morbi quis nibh pretium risus semper mollis. Phasellus luctus pharetra congue. Aliquam sed pulvinar ligula, et suscipit odio. Integer tristique nulla lectus, a tempus mi pharetra non. Donec eget urna eget erat tempus commodo. Vivamus efficitur est vel neque rutrum vestibulum. In hac habitasse platea dictumst. Integer vel mollis lorem. Donec cursus non neque eget molestie. Aliquam aliquam in massa ac feugiat. Suspendisse et nisi vel enim aliquet facilisis ut a dolor. Maecenas ornare pellentesque porta. Quisque et tempor est. Nullam sapien diam, efficitur in leo vitae, hendrerit laoreet lacus.&lt;/p&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/typography.postcss</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
</section>