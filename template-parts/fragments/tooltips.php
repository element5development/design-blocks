<section class="design-block">
	<h2>Tooltips</h2>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.on-top</h3>
			<p class="subheader">modifer to display the tooltip at the top of parent</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="tooltip-parent">
				Top
				<div class="tooltip on-top">This is the message</div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="tooltip-parent"&gt;
						&lt;div class="tooltip on-top"&gt;&lt;/div&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/tooltips.postcss</li>
					<li>../master/tooltips.js</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.on-right</h3>
			<p class="subheader">modifer to display the tooltip at the right of parent</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="tooltip-parent">
				Right
				<div class="tooltip on-right">This is the message</div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="tooltip-parent"&gt;
						&lt;div class="tooltip on-top"&gt;&lt;/div&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/tooltips.postcss</li>
					<li>../master/tooltips.js</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.on-bottom</h3>
			<p class="subheader">modifer to display the tooltip at the bottom of parent</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="tooltip-parent">
				Bottom
				<div class="tooltip on-bottom">This is the message</div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="tooltip-parent"&gt;
						&lt;div class="tooltip on-top"&gt;&lt;/div&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/tooltips.postcss</li>
					<li>../master/tooltips.js</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
	<div class="modifer">
		<div class="modifer-header">
			<h3>.on-left</h3>
			<p class="subheader">modifer to display the tooltip at the left of parent</p>
			<svg>
				<use xlink:href="#code" />
			</svg>
		</div>
		<div class="modifer-example">
			<div class="tooltip-parent">
				Left
				<div class="tooltip on-left">This is the message</div>
			</div>
		</div>
		<div class="modifer-annotation">
			<pre>
				<code class="language-html">
					&lt;div class="tooltip-parent"&gt;
						&lt;div class="tooltip on-top"&gt;&lt;/div&gt;
					&lt;/div&gt;
				</code>
			</pre>
			<div class="modifer-notes">
				<p>
					<b>Referanced File Paths:</b>
				</p>
				<ul>
					<li>../fragments/tooltips.postcss</li>
					<li>../master/tooltips.js</li>
				</ul>
				<p>
					<b>Additional Notes:</b>
				</p>
				<p>...</p>
			</div>
		</div>
	</div>
</section>