<?php 
/*-----------------------------------------------------------------*\

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

\*----------------------------------------------------------------*/
?>

<?php $file_includes = [
  'lib/theme_support.php',          // Enable Theme Options
  'lib/soil_setup.php',             // Enable Soil Features
  'lib/gf_setup.php',               // Change Submit Input to Buttons
];

foreach ($file_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'starting-point'), $file), E_USER_ERROR);
  }
  require_once $filepath;
}
unset($file, $filepath);
