var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
			MODAL/DIALOG
	\*----------------------------------------------------------------*/
	$('button.open-modal').on('click', function () {
		$(this).next().removeClass('is-hidden');
		var current = $(window).scrollTop();
		$(window).scroll(function () {
			$(window).scrollTop(current);
		});
	});
	$('.modal button.close').on('click', function () {
		$(this).parents('.modal').addClass('is-hidden');
		$(window).off('scroll');
	});
	$('.modal').on('click', function (e) {
		$(this).addClass('is-hidden');
	}).on('click', 'div', function (e) {
		e.stopPropagation();
	});
	/*----------------------------------------------------------------*\
			MODAL/DIALOG INLINE
	\*----------------------------------------------------------------*/
	$('.inline-modal button.close').on('click', function () {
		$(this).parents('.inline-modal').removeAttr('data-emergence');
	});
	$('.inline-modal + .overlay').on('click', function () {
		$(this).prev('.inline-modal').removeAttr('data-emergence');
	});
	emergence.init({
		elemCushion: 0.9,
		offsetTop: 100,
		offsetRight: 100,
		offsetBottom: 100,
		offsetLeft: 100,
	});
});