var $ = jQuery;

$(document).ready(function() {
  /*----------------------------------------------------------------*\
			ACTIVATE TOOLTIP
	\*----------------------------------------------------------------*/
  $('.tooltip-parent').hover(
    function() {
      $('.tooltip', this).addClass('is-active');
    },
    function() {
      $('.tooltip', this).removeClass('is-active');
    }
  );
});