var $ = jQuery;

$(document).ready(function() {
  /*----------------------------------------------------------------*\
			INPUT ADDING AND REMVOING CLASSES
	\*----------------------------------------------------------------*/
  $('input[type=file]').focus(function() {
    //nothing
  }).blur(function() {
    if ($(this).val().length > 0) {
      $(this).addClass('LV_valid_field');
    } else {
      $(this).removeClass('LV_valid_field');
    }
  });
  $('textarea').focus(function() {
    //nothing
  }).blur(function() {
    if ($(this).val().length > 0) {
      $(this).addClass('LV_valid_field');
    } else {
      $(this).removeClass('LV_valid_field');
    }
  });
  $('select').focus(function() {
    //nothing
  }).blur(function() {
    if ($(this).val().length > 0) {
      $(this).addClass('LV_valid_field');
    } else {
      $(this).removeClass('LV_valid_field');
    }
  });

});