var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
			SLIDERS
	\*----------------------------------------------------------------*/
	new Glide('.glide').mount()
	/*----------------------------------------------------------------*\
			ICON SLIDERS
	\*----------------------------------------------------------------*/
	new Glide('.icon-glide', {
		type: 'carousel',
		startAt: 0,
		perView: 5,
		focusAt: 'center',
		gap: 25,
		autoplay: 3000,
		breakpoints: {
			800: {
				perView: 3,
			},
			500: {
				perView: 1,
			}
		}
	}).mount()
});