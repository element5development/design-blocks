var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
			TOGGLE ANNOTATION
	\*----------------------------------------------------------------*/
	$('.modifer-header > svg').on('click', function () {
		$(this).parent().siblings('.modifer-example').toggleClass('is-active');
		$(this).parent().siblings('.modifer-annotation').toggleClass('is-active');
	});
});