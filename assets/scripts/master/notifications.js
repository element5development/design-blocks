var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
			NOTIFICATIONS
	\*----------------------------------------------------------------*/
	$('.notification button.close').on('click', function () {
		$(this).parent('.notification').addClass('is-hidden');
	});
});