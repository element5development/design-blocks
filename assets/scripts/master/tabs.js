var $ = jQuery;

$(document).ready(function() {
  /*----------------------------------------------------------------*\
			ACTIVE AND HIDE TAB CONTENT
	\*----------------------------------------------------------------*/
  $('.tabs nav button').on('click', function() {
    var index = $(this).index() + 1;
    $('.tabs nav button').removeClass('is-active');
    $(this).addClass('is-active');
    $('.tab-contents .tab').removeClass('is-active');
    $('.tab-contents .tab:nth-child(' + index + ')').addClass('is-active');
  });
});