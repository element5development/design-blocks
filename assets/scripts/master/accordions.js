var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
			ACCORDIONS
	\*----------------------------------------------------------------*/
	$('.accordion-label').on('click', function () {
		if ($(this).next('.accordion-contents').hasClass('is-active')) {
			$(this).removeClass('is-active');
			$(this).next('.accordion-contents').removeClass('is-active');
		} else {
			if ($(this).parent('.accordion').hasClass('has-one-active')) {
				$(this).siblings().removeClass('is-active');
			}
			$(this).next('.accordion-contents').addClass('is-active');
			$(this).addClass('is-active');
		}
	});
});