var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
			GALLERIES
	\*----------------------------------------------------------------*/
	$('.gallery .item').featherlightGallery({
		previousIcon: '«',
		nextIcon: '»',
		galleryFadeIn: 100,
		galleryFadeOut: 300
	});
	/*----------------------------------------------------------------*\
			MASONRY GALLERIES 
	\*----------------------------------------------------------------*/
	$('.gallery.is-masonry').masonry({
		columnWidth: '.sizer',
		itemSelector: '.item',
		percentPosition: true
	});
});