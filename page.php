<?php 
/*-----------------------------------------------------------------*\

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<main>

	<?php //get_template_part('template-parts/sections/design-system-intro'); ?>

	<?php get_template_part('template-parts/fragments/grids'); ?>

	<?php get_template_part('template-parts/fragments/typography'); ?>

	<?php get_template_part('template-parts/fragments/buttons'); ?>

	<?php get_template_part('template-parts/fragments/chips'); ?>

	<?php get_template_part('template-parts/fragments/lists'); ?>

	<?php get_template_part('template-parts/fragments/navigation'); ?>

	<?php get_template_part('template-parts/fragments/forms'); ?>

	<?php get_template_part('template-parts/fragments/tables'); ?>

	<?php get_template_part('template-parts/fragments/tooltips'); ?>

	<?php get_template_part('template-parts/elements/cards'); ?>

	<?php get_template_part('template-parts/elements/notifications'); ?>

	<?php get_template_part('template-parts/elements/tabs'); ?>

	<?php get_template_part('template-parts/elements/accordions'); ?>

	<?php  get_template_part('template-parts/elements/banners'); ?>

	<?php  get_template_part('template-parts/elements/galleries'); ?>

	<?php  get_template_part('template-parts/elements/sliders'); ?>

	<?php  get_template_part('template-parts/elements/modals'); ?>

</main>

<?php get_footer(); ?>