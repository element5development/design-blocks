<?php

/*---------------------------------------*\
  INCLUDE CSS AND JS
\*---------------------------------------*/
function wp_main_assets() {
  wp_enqueue_style( 'style-name', get_stylesheet_uri() );
  wp_enqueue_style('main', get_template_directory_uri() . '/dist/styles/main.css', array(), '1.1', 'all');
  wp_enqueue_script('vendors', get_template_directory_uri() . '/dist/scripts/vendors/vendors.js', array (), 1.1, true);
  wp_enqueue_script('main', get_template_directory_uri() . '/dist/scripts/master/main.js', array ( 'jquery' ), 1.1, true);
}
add_action('wp_enqueue_scripts', 'wp_main_assets');

/*---------------------------------------*\
  Enable HTML5 Markup Support
\*---------------------------------------*/
add_theme_support('html5', array(
	'caption', 
	'comment-form', 
	'comment-list', 
	'gallery', 
	'search-form'
));

/*---------------------------------------*\
  Menus
\*---------------------------------------*/
function navigations() {
	$locations = array(
		'standard' => __( 'basic navigation menu', 'textdomain' ),
		'simple' => __( 'simple navigation menu', 'textdomain' ),
	);
	register_nav_menus( $locations );
}
add_action( 'init', 'navigations' );