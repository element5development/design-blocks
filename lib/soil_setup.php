<?php

/*---------------------------------------*\
  Enable Soil Features
\*---------------------------------------*/
add_theme_support('soil-clean-up');
add_theme_support('soil-nav-walker');
add_theme_support('soil-nice-search');
add_theme_support('soil-jquery-cdn');
add_theme_support('soil-relative-urls');